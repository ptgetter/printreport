﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PrintControl;
using PrintCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace PrintReport
{
    public partial class Form1 : Form
    {
        List<ReportApplication> applications;
        LoadingForm loading;
        ReportBrowser reportBrowser;
        ManualResetEvent exportEvent;

        string outputPath;

        string dataFormatPath;
        string masterFormatPath;

        string acrobatPath;

        int currentIndex = -1;
        bool isMore = false;
        int currentPage = 1;

        int reportCnt = 0;
        int statCnt = 0;

        List<string> errorReport;

        public Form1()
        {
            InitializeComponent();
            SetIEVer();
            LoginForm loginForm = new LoginForm();

            if (loginForm.ShowDialog() == DialogResult.OK)
            {
                this.Visible = true;
                this.ShowInTaskbar = true;
                exportEvent = new ManualResetEvent(false);
                outputPath = Path.Combine(Environment.CurrentDirectory, "PDFOUTPUT");

                loading = new LoadingForm();
                applications = new List<ReportApplication>();
                reportView.SelectEvent += ReportView_SelectEvent;
                reportView.CheckEvent += ReportView_CheckEvent;
                reportView.BottomEvent += ReportView_BottomEvent;
                studentView.OnBrowserEvent += Open_BrowserEvent;
                studentView.CheckEvent += StudentView_CheckEvent;
                defaultInfoView.OnBrowserEvent += Open_BrowserEvent;
                defaultInfoView.CheckEvent += DefaultInfoView_CheckEvent;

                searchStart_Calendar.DateString = DateTime.Now.ToShortDateString();
                searchEnd_Calendar.DateString = DateTime.Now.AddDays(1).ToShortDateString();

                if (loginForm.isAdmin)
                {
                    searchTB.Show();
                    reportView.SetAdmin();
                }

                try
                {
                    var acroKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\AcroRd32.exe");
                    acrobatPath = acroKey.GetValue("").ToString();
                    reportPrintToolStripMenuItem.Enabled = true;
                    statPrintToolStripMenuItem.Enabled = true;
                }
                catch (Exception ex)
                {
                    LogUtil.Instance.WriteTextLog("Acrobat Reader 경로를 찾을 수 없습니다.", ex);
                    MessageBox.Show("Acrobat Reader가 없어 바로 인쇄가 불가능합니다.\r\nPDF 파일로 내보낸 후 파일을 인쇄해 주세요.");
                }

                errorReport = new List<string>();
            }
            else
            {
                Process.GetCurrentProcess().Kill();
            }
        }

        private void ReportView_BottomEvent()
        {
            if (isMore)
            {
                currentPage++;
                SearchApplication(currentPage);
            }
        }

        private void DefaultInfoView_CheckEvent(string idx, bool check, bool current, bool needAll = false)
        {
            if (applications.Count > 0)
            {
                try
                {
                    int tempCnt = applications[currentIndex].Defaults.Sum(x => x.IsCheck ? 1 : 0);
                    int cIndex = applications[currentIndex].Defaults.FindIndex(x => x.OrgIdx == idx.Split('|')[0] && x.SchoolYear == idx.Split('|')[1] && x.Class == idx.Split('|')[2]);
                    applications[currentIndex].Defaults[cIndex].IsCheck = check;

                    if (applications[currentIndex].Students.Any(x => x.IsCheck == false) || applications[currentIndex].Defaults.Any(x => x.IsCheck == false))
                    {
                        if (applications[currentIndex].IsCheck)
                        {
                            applications[currentIndex].IsCheck = false;
                            reportView.RefreshMarker(applications[currentIndex].ApplicationOrgs.First().OrgIdx, false);
                        }
                    }
                    else
                    {
                        if (!applications[currentIndex].IsCheck)
                        {
                            applications[currentIndex].IsCheck = true;
                            reportView.RefreshMarker(applications[currentIndex].ApplicationOrgs.First().OrgIdx, true);
                        }
                    }

                    statCnt = statCnt + (applications[currentIndex].Defaults.Sum(x => x.IsCheck ? 1 : 0) - tempCnt);
                    selectCntLB.Text = string.Format("선택 : 리포트 {0}개 / 통계 {1}개", reportCnt, statCnt);
                }
                catch (Exception ex)
                {
                    LogUtil.Instance.WriteTextLog("DefaultInfoView_CheckEvent", ex);
                }
            }
        }

        private void StudentView_CheckEvent(string idx, bool check, bool current, bool needAll = false)
        {
            if (applications.Count > 0)
            {
                try
                {
                    int tempCnt = applications[currentIndex].Students.Sum(x => x.IsCheck ? 1 : 0);
                    int cIndex = applications[currentIndex].Students.FindIndex(x => x.StuIdx == idx);
                    applications[currentIndex].Students[cIndex].IsCheck = check;

                    // all check 아닌 상태
                    if (applications[currentIndex].Students.Any(x => x.IsCheck == false) || applications[currentIndex].Defaults.Any(x => x.IsCheck == false))
                    {
                        if (applications[currentIndex].IsCheck)
                        {
                            applications[currentIndex].IsCheck = false;
                            reportView.RefreshMarker(applications[currentIndex].ApplicationOrgs.First().OrgIdx, false);
                        }
                    }
                    // all check 상태
                    else
                    {
                        if (!applications[currentIndex].IsCheck)
                        {
                            applications[currentIndex].IsCheck = true;
                            reportView.RefreshMarker(applications[currentIndex].ApplicationOrgs.First().OrgIdx, true);
                        }
                    }

                    reportCnt = reportCnt + (applications[currentIndex].Students.Sum(x => x.IsCheck ? 1 : 0) - tempCnt);
                    selectCntLB.Text = string.Format("선택 : 리포트 {0}개 / 통계 {1}개", reportCnt, statCnt);
                }
                catch (Exception ex)
                {
                    LogUtil.Instance.WriteTextLog("StudentView_CheckEvent", ex);
                }
            }
        }

        private void ReportView_CheckEvent(string orgIdx, bool check, bool current, bool needAll)
        {
            if (applications.Count > 0)
            {
                try
                {
                    int cIndex = applications.FindIndex(x => x.ApplicationOrgs.FirstOrDefault()?.OrgIdx == orgIdx);
                    applications[cIndex].IsCheck = check;

                    if (applications[cIndex].Students == null)
                    {
                        // 데이터 받아오기
                        var serializer = new JavaScriptSerializer();
                        var jsonData = new
                        {
                            orgIdx = applications[cIndex].ApplicationOrgs.FirstOrDefault()?.OrgIdx
                        };

                        var student = JsonConvert.DeserializeObject<List<ReportStudent>>(WebUtil.Instance.RequestPost("Exam/api/ExamStudent", serializer.Serialize(jsonData))); 
                        if (applications[cIndex].ApplicationOrgs.First().ExamCode.ToUpper() == "NA")
                        {
                            List<ReportStudent> studentParent = new List<ReportStudent>();
                            foreach (var item in student)
                            {
                                
                                var cloneItem = item.Clone() as ReportStudent;
                                item.Parent = 'F';
                                cloneItem.Parent = 'M';
                                studentParent.Add(item);
                                studentParent.Add(cloneItem);
                            }

                            student = studentParent;
                        }

                        applications[cIndex].Students = student;
                    }

                    if (applications[cIndex].Defaults == null)
                    {
                        // 데이터 받아오기
                        var serializer = new JavaScriptSerializer();
                        var jsonData = new
                        {
                            orgIdx = applications[cIndex].ApplicationOrgs.FirstOrDefault()?.OrgIdx
                        };
                        applications[cIndex].Defaults = JsonConvert.DeserializeObject<List<ReportDefaultInfo>>(WebUtil.Instance.RequestPost("Exam/api/ExamDefaultInfo", serializer.Serialize(jsonData)));
                    }

                    if (needAll || check || (!applications[cIndex].Students.Any(x => x.IsCheck == false) && !applications[cIndex].Defaults.Any(x => x.IsCheck == false)))
                    {
                        int tempStuCnt = applications[cIndex].Students.Sum(x => x.IsCheck ? 1 : 0);
                        int tempDefCnt = applications[cIndex].Defaults.Sum(x => x.IsCheck ? 1 : 0);

                        applications[cIndex].Students.Select(c => { c.IsCheck = check; return c; }).ToList();
                        applications[cIndex].Defaults.Select(c => { c.IsCheck = check; return c; }).ToList();

                        if (current)
                        {
                            studentView.RefreshMarker(check);
                            defaultInfoView.RefreshMarker(check);
                        }
                        else
                        {
                            reportCnt = reportCnt + (applications[cIndex].Students.Sum(x => x.IsCheck ? 1 : 0) - tempStuCnt);
                            statCnt = statCnt + (applications[cIndex].Defaults.Sum(x => x.IsCheck ? 1 : 0) - tempDefCnt);
                            selectCntLB.Text = string.Format("선택 : 리포트 {0}개 / 통계 {1}개", reportCnt, statCnt);
                        }
                    }

                    //applications[currentIndex].Defaults[cIndex].IsCheck = check;

                    //if (applications[currentIndex].Students.Any(x => x.IsCheck == false) || applications[currentIndex].Defaults.Any(x => x.IsCheck == false))
                    //{
                    //    if (applications[currentIndex].IsCheck)
                    //    {
                    //        applications[currentIndex].IsCheck = false;
                    //        reportView.RefreshMarker(applications[currentIndex].ApplicationOrgs.First().OrgIdx, false);
                    //    }
                    //}
                    //else
                    //{
                    //    if (!applications[currentIndex].IsCheck)
                    //    {
                    //        applications[currentIndex].IsCheck = true;
                    //        reportView.RefreshMarker(applications[currentIndex].ApplicationOrgs.First().OrgIdx, true);
                    //    }
                    //}

                    //statCnt = statCnt + (applications[currentIndex].Defaults.Sum(x => x.IsCheck ? 1 : 0) - tempCnt);
                    //selectCntLB.Text = string.Format("선택 : 리포트 {0}개 / 통계 {1}개", reportCnt, statCnt);
                }
                catch (Exception ex)
                {
                    LogUtil.Instance.WriteTextLog("ReportView_CheckEvent", ex);
                }
            }
        }

        private void Open_BrowserEvent(int type, int index)
        {
            try
            {
                //if (reportBrowser == null)
                //{
                //    reportBrowser = new ReportBrowser();
                //    reportBrowser.ShowBrowser += ReportBrowser_ShowBrowser;
                //    reportBrowser.taskComplete += ReportBrowser_taskComplete;
                //}

                ReportBrowser rBrowser = new ReportBrowser();
                if (rBrowser.Browser.InitScript())
                {
                    rBrowser.Show();
                    if (type == 0)
                    {
                        if (applications[currentIndex].Students[index].ExamCode.ToUpper() == "NA")
                        {
                            if (!rBrowser.Browser.SetNavigation(string.Format("Report/rpt{0}?stuIdx={1}&parentsType={2}", applications[currentIndex].Students[index].ExamCode.ToUpper(), applications[currentIndex].Students[index].StuIdx, applications[currentIndex].Students[index].Parent), 19))
                            {
                                rBrowser.Close();
                            }
                        }
                        else
                        {
                            if (!rBrowser.Browser.SetNavigation(string.Format("Report/rpt{0}?stuIdx={1}", applications[currentIndex].Students[index].ExamCode.ToUpper(), applications[currentIndex].Students[index].StuIdx), 19))
                            {
                                rBrowser.Close();
                            }
                        }
                    }
                    else
                    {
                        if (!rBrowser.Browser.SetNavigation(string.Format("Stats/stats{0}?o={1}&s={2}&c={3}", applications[currentIndex].Defaults[index].ExamCode.ToUpper(), applications[currentIndex].Defaults[index].OrgIdx, applications[currentIndex].Defaults[index].SchoolYear.Trim(), applications[currentIndex].Defaults[index].Class.Trim()), 19))
                        {
                            rBrowser.Close();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("스크립트 파일이 없습니다.");
                }
            }
            catch (Exception ex)
            {
                LogUtil.Instance.WriteTextLog("StudentView_BrowserEvent", ex);
            }
        }

        private void ReportBrowser_taskComplete(int type)
        {
            // 브라우져 로드 완료
            if (type == 1)
            {
                exportEvent.Set();
            }
            // pdf 저장 완료
            else if (type == 2)
            {

            }
            // 브라우져 로드 실패
            else if (type == 3)
            {
                exportEvent.Set();
            }
        }

        private void ReportView_SelectEvent(string orgIdx)
        {
            try
            {
                Point p = this.PointToScreen(Point.Empty);
                //loading.Location = new Point(p.X + (this.Width / 2) - (loading.Width / 2), p.Y + (this.Height / 2) - (loading.Height / 2));
                //ShowLoading();

                currentIndex = applications.FindIndex(x => x.ApplicationOrgs.FirstOrDefault()?.OrgIdx == orgIdx);
                List<ReportStudent> student;
                List<ReportDefaultInfo> defaultinfo;

                if (applications[currentIndex].Students == null)
                {
                    var serializer = new JavaScriptSerializer();
                    var jsonData = new
                    {
                        orgIdx = applications[currentIndex].ApplicationOrgs.FirstOrDefault()?.OrgIdx
                    };

                    student = JsonConvert.DeserializeObject<List<ReportStudent>>(WebUtil.Instance.RequestPost("Exam/api/ExamStudent", serializer.Serialize(jsonData)));
                    if (applications[currentIndex].ApplicationOrgs.First().ExamCode.ToUpper() == "NA")
                    {
                        List<ReportStudent> studentParent = new List<ReportStudent>();
                        foreach (var item in student)
                        {
                            item.Parent = 'F';
                            studentParent.Add(item);
                            item.Parent = 'M';
                            studentParent.Add(item);
                        }

                        student = studentParent;
                    }
                    applications[currentIndex].Students = new List<ReportStudent>(student);
                }
                else
                {
                    student = applications[currentIndex].Students;
                }

                if (applications[currentIndex].Defaults == null)
                {
                    var serializer = new JavaScriptSerializer();
                    var jsonData = new
                    {
                        orgIdx = applications[currentIndex].ApplicationOrgs.FirstOrDefault()?.OrgIdx
                    };

                    defaultinfo = JsonConvert.DeserializeObject<List<ReportDefaultInfo>>(WebUtil.Instance.RequestPost("Exam/api/ExamDefaultInfo", serializer.Serialize(jsonData)));
                    applications[currentIndex].Defaults = new List<ReportDefaultInfo>(defaultinfo);
                }
                else
                {
                    defaultinfo = applications[currentIndex].Defaults;
                }

                studentView.SetData(student);
                defaultInfoView.SetData(defaultinfo);

                //if (loading.IsHandleCreated)
                //{
                //    loading.Invoke(new Action(() => { loading.Close(); }));
                //}
                //else
                //{
                //    loading.Close();
                //}
            }
            catch (Exception ex)
            {
                LogUtil.Instance.WriteTextLog("세부 내용 조회 오류", ex);
            }
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime parseDate;
                if (!DateTime.TryParse(searchStart_Calendar.DateString, out parseDate))
                {
                    MessageBox.Show("검색 시작 날짜가 잘못 되었습니다.");
                    return;
                }
                else if (!DateTime.TryParse(searchEnd_Calendar.DateString, out parseDate))
                {
                    MessageBox.Show("검색 종료 날짜가 잘못 되었습니다.");
                    return;
                }

                applications.Clear();
                reportView.ClearList();

                currentPage = 1;
                if (SearchApplication(currentPage) < 1)
                {
                    MessageBox.Show("조회된 자료가 없습니다.");
                }
            }
            catch (Exception ex)
            {
                LogUtil.Instance.WriteTextLog("조회 오류", ex);
            }
        }

        private int SearchApplication(int page)
        {
            reportView.Loading(true);
            var serializer = new JavaScriptSerializer();
            var jsonData = new
            {
                startDate = searchStart_Calendar.DateString,
                endDate = searchEnd_Calendar.DateString,
                schName = searchTB.Text,
                rowcnt = 30
            };

            var jsonRet = JObject.Parse(WebUtil.Instance.RequestPost("Exam/api/ExamApplication?page=" + this.currentPage.ToString(), serializer.Serialize(jsonData)));

            var data = JsonConvert.DeserializeObject<List<ReportApplication>>(jsonRet["List"].ToString());

            if (data.Count == 30)
            {
                isMore = true;
            }
            else
            {
                isMore = false;
            }

            data = data.Where(x => x.ApplicationOrgs.Count > 0).ToList();

            applications.AddRange(data);
            reportView.SetData(data);
            reportView.Loading(false);

            return data.Count;
        }

        private void ShowLoading()
        {
            try
            {
                if (this.InvokeRequired)
                {
                    loading.Show();
                }
                else
                {
                    Thread th = new Thread(ShowLoading);
                    th.IsBackground = false;
                    th.Start();
                }
            }
            catch (Exception ex)
            {
                LogUtil.Instance.WriteTextLog("로딩 오류", ex);
            }
        }

        private void SetIEVer()
        {
            int ver = 11000;
            string ks;
            string app;
            string[] cm = Environment.GetCommandLineArgs();
            app = Path.GetFileName(cm[0]);
            if (Environment.Is64BitProcess) ks = @"SOFTWARE\Wow6432Node";
            else ks = @"SOFTWARE";
            ks += @"\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_BROWSER_EMULATION";
            try
            {
                RegistryKey key;
                key = Registry.CurrentUser.OpenSubKey(ks, true);
                if (key != null)
                {
                    if (ver != 0) key.SetValue(app, ver, RegistryValueKind.DWord);
                    else key.DeleteValue(app, false);
                    key.Close();
                }
            }
            catch (Exception ex)
            {
                LogUtil.Instance.WriteTextLog("SetIEVer", ex);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Process.GetCurrentProcess().Kill();
            }
            catch { }
        }

        private void exportPDFWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            errorReport.Clear();
            this.Invoke(new Action(delegate ()
            {
                loading.LoadingTitle = "수집 중...";
            }));
            string[] arguments = e.Argument.ToString().Split('|');

            if (Directory.Exists(outputPath))
            {
                foreach (string path in Directory.GetFiles(outputPath))
                {
                    File.Delete(path);
                }
            }

            this.Invoke(new Action(delegate ()
            {
                if (reportBrowser == null)
                {
                    reportBrowser = new ReportBrowser(false);
                    reportBrowser.Browser.taskComplete += ReportBrowser_taskComplete;
                }
                reportBrowser.Show();
                reportBrowser.Visible = false;
            }));

            int cnt = 1;
            //int totalCnt = 0;
            if (arguments[0] == "report")
            {
                //foreach (var item in applications)
                //{
                //    if (item.IsCheck)
                //    {
                //        if (item.Students == null)
                //        {
                //            // 데이터 받아오기
                //            var serializer = new JavaScriptSerializer();
                //            var jsonData = new
                //            {
                //                orgIdx = item.ApplicationOrgs.FirstOrDefault()?.OrgIdx
                //            };
                //            applications[currentIndex].Students = JsonConvert.DeserializeObject<List<ReportStudent>>(WebUtil.Instance.RequestPost("Exam/api/ExamStudent", serializer.Serialize(jsonData)));
                //        }

                //        totalCnt += item.Students.Count;
                //    }
                //    else
                //    {
                //        if (item.Students != null)
                //        {
                //            totalCnt += item.Students.Sum(x => { if (x.IsCheck) { return 1; } else { return 0; } });
                //        }
                //    }
                //}


                foreach (var item in applications)
                {
                    if (item.Students != null)
                    {
                        foreach (var report in item.Students)
                        {
                            if (item.IsCheck || report.IsCheck)
                            {
                                this.Invoke(new Action(delegate ()
                                {
                                    loading.LoadingTitle = string.Format("수집 중 {0}/{1}", cnt, reportCnt);
                                }));

                                if (reportBrowser.Browser.SetNavigationSilent(string.Format("Report/rpt{0}?stuIdx={1}", report.ExamCode.ToUpper(), report.StuIdx), 37, dataFormatPath))
                                {
                                    exportEvent.Reset();
                                    exportEvent.WaitOne();
                                    reportBrowser.Browser.SavePDF(outputPath);
                                }
                                else
                                {

                                }

                                cnt++;
                            }
                        }
                    }
                }
            }
            else if (arguments[0] == "stats")
            {
                //foreach (var item in applications)
                //{
                //    if (item.IsCheck)
                //    {
                //        if (item.Defaults == null)
                //        {
                //            // 데이터 받아오기
                //            var serializer = new JavaScriptSerializer();
                //            var jsonData = new
                //            {
                //                orgIdx = item.ApplicationOrgs.FirstOrDefault()?.OrgIdx
                //            };
                //            applications[currentIndex].Defaults = JsonConvert.DeserializeObject<List<ReportDefaultInfo>>(WebUtil.Instance.RequestPost("Exam/api/ExamDefaultInfo", serializer.Serialize(jsonData)));
                //        }

                //        totalCnt += item.Defaults.Count;
                //    }
                //    else
                //    {
                //        if (item.Defaults != null)
                //        {
                //            totalCnt += item.Defaults.Sum(x => { if (x.IsCheck) { return 1; } else { return 0; } });
                //        }
                //    }
                //}

                foreach (var item in applications)
                {
                    if (item.Students != null)
                    {
                        foreach (var stats in item.Defaults)
                        {
                            if (item.IsCheck || stats.IsCheck)
                            {
                                this.Invoke(new Action(delegate ()
                                {
                                    loading.LoadingTitle = string.Format("수집 중 {0}/{1}", cnt, statCnt);
                                }));
                                if (reportBrowser.Browser.SetNavigationSilent(string.Format("Stats/stats{0}?o={1}&s={2}&c={3}", stats.ExamCode.ToUpper(), stats.OrgIdx, stats.SchoolYear.Trim(), stats.Class.Trim()), 53, dataFormatPath))
                                {
                                    exportEvent.Reset();
                                    exportEvent.WaitOne();
                                    reportBrowser.Browser.SavePDF(outputPath);
                                }
                                else
                                {
                                    
                                }

                                cnt++;
                            }
                        }
                    }
                }
            }

            //if (arguments.Length > 1 && arguments[1] == "pdf")
            //{
            string fileName = arguments.Length > 2 ? arguments[2] : Path.Combine(Environment.CurrentDirectory, string.Concat(DateTime.Now.Ticks.ToString(), ".pdf"));
            MergePDF(outputPath, fileName);
            //}

            if (arguments.Length > 1 && arguments[1] == "print")
            {
                ProcessStartInfo psi = new ProcessStartInfo();
                psi.Verb = "print";
                psi.FileName = acrobatPath;
                psi.Arguments = string.Format(@"/t {0}", Path.Combine(Environment.CurrentDirectory, fileName));
                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.CreateNoWindow = false;
                psi.UseShellExecute = false;

                Process pdfPrint = new Process();
                pdfPrint.StartInfo = psi;
                pdfPrint.Start();

                //pdfPrint.WaitForInputIdle();
                //pdfPrint.Kill();

                //reportBrowser.Browser.SetPDFView(Path.Combine(Environment.CurrentDirectory, fileTick), true);
                //this.Invoke(new Action(() => { loading.Close(); reportBrowser.Show(); }));
                this.Invoke(new Action(() => { loading.Close(); }));
            }
            else
            {
                MessageBox.Show("내보내기 완료");
                this.Invoke(new Action(delegate ()
                {
                    reportBrowser.Close();
                    loading.Close();
                }));
            }
        }

        private void MergePDF(string filePath, string fileName)
        {
            Document document = new Document();

            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                try
                {
                    PdfCopy writer = new PdfCopy(document, fs);
                    if (writer == null)
                    {
                        return;
                    }

                    document.Open();

                    foreach (string path in Directory.GetFiles(filePath))
                    {
                        PdfReader reader = new PdfReader(path);
                        reader.ConsolidateNamedDestinations();

                        for (int i = 1; i <= reader.NumberOfPages; i++)
                        {
                            PdfImportedPage page = writer.GetImportedPage(reader, i);
                            writer.AddPage(page);
                        }

                        PRAcroForm form = reader.AcroForm;
                        if (form != null)
                        {
                            writer.CopyAcroForm(reader);
                        }

                        reader.Close();
                    }

                    writer.Close();
                    document.Close();
                }
                catch (Exception ex)
                {
                    LogUtil.Instance.WriteTextLog("MergePDF", ex);
                    MessageBox.Show("PDF를 합치는데 실패하였습니다.");
                }
            }
        }

        private void previewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //OpenFileDialog ofd = new OpenFileDialog();
            //ofd.Filter = "양식 파일|*.xml";
            //if (ofd.ShowDialog() == DialogResult.OK)
            //{

            //}

            if (!File.Exists(masterFormatPath))
            {
                MessageBox.Show("마스터 양식이 선택되지 않았습니다.");
                SetFormatPath(0);
            }

            if (!File.Exists(dataFormatPath))
            {
                MessageBox.Show("인쇄 양식이 선택되지 않았습니다.");
                SetFormatPath(1);
            }

            string reportURL;
            if (tabControl1.SelectedIndex == 0)
            {
                string tagValue = studentView.GetSelectInfo();
                if (!string.IsNullOrEmpty(tagValue))
                {
                    int cIndex = applications[currentIndex].Students.FindIndex(x => x.StuIdx == tagValue);
                    reportURL = string.Format("Report/rpt{0}?stuIdx={1}", applications[currentIndex].Students[cIndex].ExamCode.ToUpper(), applications[currentIndex].Students[cIndex].StuIdx);
                    PreviewBackForm pbf = new PreviewBackForm(reportURL, dataFormatPath, masterFormatPath);
                    pbf.Show();
                }
                else
                {
                    MessageBox.Show("미리보기 항목을 선택해 주세요.");
                }
            }
            else
            {
                string tagValue = defaultInfoView.GetSelectInfo();
                if (!string.IsNullOrEmpty(tagValue))
                {
                    int cIndex = applications[currentIndex].Defaults.FindIndex(x => x.OrgIdx == tagValue.Split('|')[0] && x.SchoolYear == tagValue.Split('|')[1] && x.Class == tagValue.Split('|')[2]);
                    reportURL = string.Format("Stats/stats{0}?o={1}&s={2}&c={3}", applications[currentIndex].Defaults[cIndex].ExamCode.ToUpper(), applications[currentIndex].Defaults[cIndex].OrgIdx, applications[currentIndex].Defaults[cIndex].SchoolYear.Trim(), applications[currentIndex].Defaults[cIndex].Class.Trim());
                    PreviewBackForm pbf = new PreviewBackForm(reportURL, dataFormatPath, masterFormatPath);
                    pbf.Show();
                }
                else
                {
                    MessageBox.Show("미리보기 항목을 선택해 주세요.");
                }
            }

            
        }

        private void statPDFExportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (statCnt < 1)
            {
                MessageBox.Show("출력할 통계를 선택해 주세요.");
                return;
            }

            if (!File.Exists(dataFormatPath))
            {
                MessageBox.Show("인쇄 양식이 선택되지 않았습니다.");
                SetFormatPath(1);
            }

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "PDF 파일|*.pdf";
            sfd.OverwritePrompt = true;
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(sfd.FileName))
                {
                    File.Delete(sfd.FileName);
                }

                if (!exportPDFWorker.IsBusy)
                {
                    exportPDFWorker.RunWorkerAsync(string.Concat("stats|pdf|", sfd.FileName));
                    Application.DoEvents();
                    loading.ShowDialog();
                }
            }

            
        }

        private void reportPDFExportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (reportCnt < 1)
            {
                MessageBox.Show("출력할 리포트를 선택해 주세요.");
                return;
            }

            if (!File.Exists(dataFormatPath))
            {
                MessageBox.Show("인쇄 양식이 선택되지 않았습니다.");
                SetFormatPath(1);
            }

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "PDF 파일|*.pdf";
            sfd.OverwritePrompt = true;
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(sfd.FileName))
                {
                    File.Delete(sfd.FileName);
                }

                if (!exportPDFWorker.IsBusy)
                {
                    exportPDFWorker.RunWorkerAsync(string.Concat("report|pdf|", sfd.FileName));
                    Application.DoEvents();
                    loading.ShowDialog();
                }
            }
        }

        private void statPrintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (statCnt < 1)
            {
                MessageBox.Show("통계 리포트를 선택해 주세요.");
                return;
            }

            if (!File.Exists(dataFormatPath))
            {
                MessageBox.Show("인쇄 양식이 선택되지 않았습니다.");
                SetFormatPath(1);
            }

            if (!exportPDFWorker.IsBusy)
            {
                exportPDFWorker.RunWorkerAsync("stats|print|");
                Application.DoEvents();
                loading.ShowDialog();
            }
        }

        private void reportPrintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (reportCnt < 1)
            {
                MessageBox.Show("출력할 리포트를 선택해 주세요.");
                return;
            }

            if (!File.Exists(dataFormatPath)) 
            {
                MessageBox.Show("인쇄 양식이 선택되지 않았습니다.");
                SetFormatPath(1);
            }

            if (!exportPDFWorker.IsBusy)
            {
                exportPDFWorker.RunWorkerAsync("report|print");
                Application.DoEvents();
                loading.ShowDialog();
            }
        }

        private void masterPathBtn_Click(object sender, EventArgs e)
        {
            SetFormatPath(0);
        }

        private void dataPathBtn_Click(object sender, EventArgs e)
        {
            SetFormatPath(1);
        }

        private void searchTB_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                searchBtn_Click(null, null);
            }
        }

        private string GetFileName(string filePath)
        {
            try
            {
                FileInfo fileinfo = new FileInfo(filePath);

                return fileinfo.Name;
            }
            catch (Exception ex)
            {
                LogUtil.Instance.WriteTextLog("GetFileName", ex);
                return "선택안됨";
            }
        }

        private void SetFormatPath(int type)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (type == 0)
            {
                ofd.Filter = "마스터 파일|*.pdf";
            }
            else
            {
                ofd.Filter = "양식 파일|*.xml";
            }
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                if (type == 0)
                {
                    masterFormatPath = ofd.FileName;
                    masterPathLB.Text = GetFileName(masterFormatPath);
                }
                else if (type == 1)
                {
                    dataFormatPath = ofd.FileName;
                    dataPathLB.Text = GetFileName(dataFormatPath);
                }
            }
        }

        private void selectClearBtn_Click(object sender, EventArgs e)
        {
            reportView.ClearMarker(applications[currentIndex].ApplicationOrgs.First().OrgIdx);
            reportCnt = 0;
            statCnt = 0;
            selectCntLB.Text = string.Format("선택 : 리포트 {0}개 / 통계 {1}개", reportCnt, statCnt);
        }

        private void panel3_MouseClick(object sender, MouseEventArgs e)
        {
            searchTB.Focus();
        }
    }
}
