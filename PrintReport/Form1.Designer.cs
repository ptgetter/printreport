﻿namespace PrintReport
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchBtn = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.studentPage = new System.Windows.Forms.TabPage();
            this.defaultPage = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.previewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.인쇄ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportPrintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statPrintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportPDFExportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statPDFExportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportPDFWorker = new System.ComponentModel.BackgroundWorker();
            this.masterPathBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.masterPathLB = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataPathBtn = new System.Windows.Forms.Button();
            this.dataPathLB = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.searchTB = new System.Windows.Forms.TextBox();
            this.selectClearBtn = new System.Windows.Forms.Button();
            this.selectCntLB = new System.Windows.Forms.Label();
            this.searchStart_Calendar = new PrintControl.SearchCalendar();
            this.searchEnd_Calendar = new PrintControl.SearchCalendar();
            this.studentView = new PrintControl.StudentView();
            this.defaultInfoView = new PrintControl.DefaultInfoView();
            this.reportView = new PrintControl.ReportView();
            this.tabControl1.SuspendLayout();
            this.studentPage.SuspendLayout();
            this.defaultPage.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(594, 70);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(75, 36);
            this.searchBtn.TabIndex = 1;
            this.searchBtn.Text = "조회";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.studentPage);
            this.tabControl1.Controls.Add(this.defaultPage);
            this.tabControl1.Location = new System.Drawing.Point(676, 93);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(348, 444);
            this.tabControl1.TabIndex = 2;
            // 
            // studentPage
            // 
            this.studentPage.Controls.Add(this.studentView);
            this.studentPage.Location = new System.Drawing.Point(4, 22);
            this.studentPage.Name = "studentPage";
            this.studentPage.Padding = new System.Windows.Forms.Padding(3);
            this.studentPage.Size = new System.Drawing.Size(340, 418);
            this.studentPage.TabIndex = 0;
            this.studentPage.Text = "리포트";
            this.studentPage.UseVisualStyleBackColor = true;
            // 
            // defaultPage
            // 
            this.defaultPage.Controls.Add(this.defaultInfoView);
            this.defaultPage.Location = new System.Drawing.Point(4, 22);
            this.defaultPage.Name = "defaultPage";
            this.defaultPage.Padding = new System.Windows.Forms.Padding(3);
            this.defaultPage.Size = new System.Drawing.Size(340, 418);
            this.defaultPage.TabIndex = 1;
            this.defaultPage.Text = "통계";
            this.defaultPage.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(236, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "~";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.인쇄ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1025, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.previewToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.fileToolStripMenuItem.Text = "파일";
            // 
            // previewToolStripMenuItem
            // 
            this.previewToolStripMenuItem.Name = "previewToolStripMenuItem";
            this.previewToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.previewToolStripMenuItem.Text = "미리보기";
            this.previewToolStripMenuItem.Click += new System.EventHandler(this.previewToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.exitToolStripMenuItem.Text = "종료";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // 인쇄ToolStripMenuItem
            // 
            this.인쇄ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportPrintToolStripMenuItem,
            this.statPrintToolStripMenuItem,
            this.reportPDFExportToolStripMenuItem,
            this.statPDFExportToolStripMenuItem});
            this.인쇄ToolStripMenuItem.Name = "인쇄ToolStripMenuItem";
            this.인쇄ToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.인쇄ToolStripMenuItem.Text = "인쇄";
            // 
            // reportPrintToolStripMenuItem
            // 
            this.reportPrintToolStripMenuItem.Enabled = false;
            this.reportPrintToolStripMenuItem.Name = "reportPrintToolStripMenuItem";
            this.reportPrintToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.reportPrintToolStripMenuItem.Text = "리포트 인쇄";
            this.reportPrintToolStripMenuItem.Click += new System.EventHandler(this.reportPrintToolStripMenuItem_Click);
            // 
            // statPrintToolStripMenuItem
            // 
            this.statPrintToolStripMenuItem.Enabled = false;
            this.statPrintToolStripMenuItem.Name = "statPrintToolStripMenuItem";
            this.statPrintToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.statPrintToolStripMenuItem.Text = "통계 인쇄";
            this.statPrintToolStripMenuItem.Click += new System.EventHandler(this.statPrintToolStripMenuItem_Click);
            // 
            // reportPDFExportToolStripMenuItem
            // 
            this.reportPDFExportToolStripMenuItem.Name = "reportPDFExportToolStripMenuItem";
            this.reportPDFExportToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.reportPDFExportToolStripMenuItem.Text = "리포트 PDF 저장";
            this.reportPDFExportToolStripMenuItem.Click += new System.EventHandler(this.reportPDFExportToolStripMenuItem_Click);
            // 
            // statPDFExportToolStripMenuItem
            // 
            this.statPDFExportToolStripMenuItem.Name = "statPDFExportToolStripMenuItem";
            this.statPDFExportToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.statPDFExportToolStripMenuItem.Text = "통계 PDF 저장";
            this.statPDFExportToolStripMenuItem.Click += new System.EventHandler(this.statPDFExportToolStripMenuItem_Click);
            // 
            // exportPDFWorker
            // 
            this.exportPDFWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.exportPDFWorker_DoWork);
            // 
            // masterPathBtn
            // 
            this.masterPathBtn.Location = new System.Drawing.Point(206, 7);
            this.masterPathBtn.Name = "masterPathBtn";
            this.masterPathBtn.Size = new System.Drawing.Size(54, 23);
            this.masterPathBtn.TabIndex = 9;
            this.masterPathBtn.Text = "선택";
            this.masterPathBtn.UseVisualStyleBackColor = true;
            this.masterPathBtn.Click += new System.EventHandler(this.masterPathBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(113, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 10;
            this.label2.Text = "마스터양식";
            // 
            // masterPathLB
            // 
            this.masterPathLB.AutoSize = true;
            this.masterPathLB.Location = new System.Drawing.Point(3, 12);
            this.masterPathLB.Name = "masterPathLB";
            this.masterPathLB.Size = new System.Drawing.Size(53, 12);
            this.masterPathLB.TabIndex = 11;
            this.masterPathLB.Text = "선택안됨";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.masterPathBtn);
            this.panel1.Controls.Add(this.masterPathLB);
            this.panel1.Location = new System.Drawing.Point(110, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(265, 36);
            this.panel1.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(407, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 13;
            this.label4.Text = "인쇄양식";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.dataPathBtn);
            this.panel2.Controls.Add(this.dataPathLB);
            this.panel2.Location = new System.Drawing.Point(404, 27);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(265, 36);
            this.panel2.TabIndex = 14;
            // 
            // dataPathBtn
            // 
            this.dataPathBtn.Location = new System.Drawing.Point(206, 7);
            this.dataPathBtn.Name = "dataPathBtn";
            this.dataPathBtn.Size = new System.Drawing.Size(54, 23);
            this.dataPathBtn.TabIndex = 9;
            this.dataPathBtn.Text = "선택";
            this.dataPathBtn.UseVisualStyleBackColor = true;
            this.dataPathBtn.Click += new System.EventHandler(this.dataPathBtn_Click);
            // 
            // dataPathLB
            // 
            this.dataPathLB.AutoSize = true;
            this.dataPathLB.Location = new System.Drawing.Point(3, 12);
            this.dataPathLB.Name = "dataPathLB";
            this.dataPathLB.Size = new System.Drawing.Size(53, 12);
            this.dataPathLB.TabIndex = 11;
            this.dataPathLB.Text = "선택안됨";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.searchTB);
            this.panel3.Location = new System.Drawing.Point(485, 70);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(100, 36);
            this.panel3.TabIndex = 15;
            this.panel3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseClick);
            // 
            // searchTB
            // 
            this.searchTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.searchTB.Location = new System.Drawing.Point(1, 10);
            this.searchTB.Name = "searchTB";
            this.searchTB.Size = new System.Drawing.Size(96, 14);
            this.searchTB.TabIndex = 0;
            this.searchTB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.searchTB_KeyPress);
            // 
            // selectClearBtn
            // 
            this.selectClearBtn.Location = new System.Drawing.Point(909, 62);
            this.selectClearBtn.Name = "selectClearBtn";
            this.selectClearBtn.Size = new System.Drawing.Size(99, 27);
            this.selectClearBtn.TabIndex = 16;
            this.selectClearBtn.Text = "선택해제";
            this.selectClearBtn.UseVisualStyleBackColor = true;
            this.selectClearBtn.Click += new System.EventHandler(this.selectClearBtn_Click);
            // 
            // selectCntLB
            // 
            this.selectCntLB.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.selectCntLB.Location = new System.Drawing.Point(677, 29);
            this.selectCntLB.Name = "selectCntLB";
            this.selectCntLB.Size = new System.Drawing.Size(336, 30);
            this.selectCntLB.TabIndex = 17;
            this.selectCntLB.Text = "선택 : 리포트 0개 / 통계 0개";
            this.selectCntLB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // searchStart_Calendar
            // 
            this.searchStart_Calendar.BackColor = System.Drawing.Color.Transparent;
            this.searchStart_Calendar.DateString = "2021-02-19";
            this.searchStart_Calendar.Location = new System.Drawing.Point(12, 70);
            this.searchStart_Calendar.Name = "searchStart_Calendar";
            this.searchStart_Calendar.Size = new System.Drawing.Size(221, 36);
            this.searchStart_Calendar.TabIndex = 6;
            this.searchStart_Calendar.Title = "시작일";
            // 
            // searchEnd_Calendar
            // 
            this.searchEnd_Calendar.BackColor = System.Drawing.Color.Transparent;
            this.searchEnd_Calendar.DateString = "2021-02-19";
            this.searchEnd_Calendar.Location = new System.Drawing.Point(254, 70);
            this.searchEnd_Calendar.Name = "searchEnd_Calendar";
            this.searchEnd_Calendar.Size = new System.Drawing.Size(221, 36);
            this.searchEnd_Calendar.TabIndex = 4;
            this.searchEnd_Calendar.Title = "종료일";
            // 
            // studentView
            // 
            this.studentView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.studentView.Location = new System.Drawing.Point(0, 0);
            this.studentView.Name = "studentView";
            this.studentView.Size = new System.Drawing.Size(340, 418);
            this.studentView.TabIndex = 0;
            // 
            // defaultInfoView
            // 
            this.defaultInfoView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.defaultInfoView.Location = new System.Drawing.Point(0, 0);
            this.defaultInfoView.Name = "defaultInfoView";
            this.defaultInfoView.Size = new System.Drawing.Size(333, 418);
            this.defaultInfoView.TabIndex = 0;
            // 
            // reportView
            // 
            this.reportView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reportView.Location = new System.Drawing.Point(4, 111);
            this.reportView.Name = "reportView";
            this.reportView.Size = new System.Drawing.Size(667, 426);
            this.reportView.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 539);
            this.Controls.Add(this.selectCntLB);
            this.Controls.Add(this.selectClearBtn);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.searchStart_Calendar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.searchEnd_Calendar);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.searchBtn);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.reportView);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(1041, 578);
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.studentPage.ResumeLayout(false);
            this.defaultPage.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PrintControl.ReportView reportView;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage studentPage;
        private System.Windows.Forms.TabPage defaultPage;
        private PrintControl.StudentView studentView;
        private PrintControl.DefaultInfoView defaultInfoView;
        private PrintControl.SearchCalendar searchEnd_Calendar;
        private System.Windows.Forms.Label label1;
        private PrintControl.SearchCalendar searchStart_Calendar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem previewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker exportPDFWorker;
        private System.Windows.Forms.ToolStripMenuItem 인쇄ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportPrintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statPrintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportPDFExportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statPDFExportToolStripMenuItem;
        private System.Windows.Forms.Button masterPathBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label masterPathLB;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button dataPathBtn;
        private System.Windows.Forms.Label dataPathLB;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox searchTB;
        private System.Windows.Forms.Button selectClearBtn;
        private System.Windows.Forms.Label selectCntLB;
    }
}

