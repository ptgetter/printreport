﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintCore
{
    public class LogUtil
    {
        #region Singleton Instance
        private static readonly Lazy<LogUtil> _instance = new Lazy<LogUtil>(() => new LogUtil());

        public static LogUtil Instance
        {
            get
            {
                return _instance.Value;
            }
        }
        #endregion

        private readonly NLog.Logger logger;

        public LogUtil()
        {
            logger = NLog.LogManager.GetCurrentClassLogger();
        }

        public void WriteTextLog(string msg, bool isError = false)
        {
            if (isError)
            {
                logger.Error(msg);
            }
            else
            {
                logger.Debug(msg);
            }

            Console.WriteLine(msg);
        }

        public void WriteTextLog(string msg, Exception ex)
        {
            logger.Error(ex, msg);
            Console.WriteLine(ex.ToString());
        }
    }
}
