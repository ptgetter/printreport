﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PrintCore
{
    public class WebUtil
    {
        #region Singleton Instance
        private static readonly Lazy<WebUtil> _instance = new Lazy<WebUtil>(() => new WebUtil());

        public static WebUtil Instance
        {
            get
            {
                return _instance.Value;
            }
        }
        #endregion

        string baseURL;
        string token { get; set; }
        public bool isLogin { get; private set; }

        public WebUtil()
        {
            baseURL = ConfigurationManager.AppSettings["URL"].ToString();
            token = string.Empty;
            isLogin = false;
        }

        #region Public

        #endregion

        public bool Login(string id, string pwd)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Concat(baseURL, "oauth2/token"));
                HttpWebResponse response = null;

                string postData = string.Format("grant_type=password&username={0}&password={1}&variable={2}", id, pwd, "exam");
                byte[] data = Encoding.UTF8.GetBytes(postData);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(data, 0, data.Length);
                    requestStream.Close();

                    response = (HttpWebResponse)request.GetResponse();
                }

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader tReader = new StreamReader(response.GetResponseStream()))
                    {
                        var info = JObject.Parse(tReader.ReadToEnd());
                        token = string.Format("{0} {1}", info["token_type"].Value<string>(), info["access_token"].Value<string>());
                        isLogin = true;
                    }
                }
                else
                {
                    LogUtil.Instance.WriteTextLog("Login fail");
                }

                return isLogin;
            }
            catch (Exception ex)
            {
                LogUtil.Instance.WriteTextLog("Login Error", ex);
                return false;
            }
        }

        public string RequestPost(string pMethod, string pData)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Concat(baseURL, pMethod));
                HttpWebResponse response = null;

                byte[] data = Encoding.UTF8.GetBytes(pData);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;
                request.Headers.Add(HttpRequestHeader.Authorization, token);

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(data, 0, data.Length);
                    requestStream.Close();

                    response = (HttpWebResponse)request.GetResponse();
                }

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader tReader = new StreamReader(response.GetResponseStream()))
                    {
                        return tReader.ReadToEnd();
                    }
                }
                else
                {
                    return response.StatusDescription;
                }
            }
            catch (Exception ex)
            {
                LogUtil.Instance.WriteTextLog("Webapi request failed", ex);
                return null;
            }
        }
    }
}
