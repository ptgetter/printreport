﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintCore
{
    public delegate void SelectIndexHandler(string orgIdx);

    public delegate void ViewBrowserHandler(int type, int index);

    public delegate void LoadBrowserHandler(bool open = true);

    public delegate void CheckBoxHandler(string idx, bool check, bool currentItem = false, bool needAll = false);

    public delegate void BrowserTaskCompleteHandler(int type);

    public delegate void ScrollPositionHandler(int x, int y);

    public delegate void ActiveFormHandler();

    public delegate void ScrollHandler();

    public delegate void ScrollBottomHandler();
}
