﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintCore
{
    public class ReportApplication
    {
        public string Name { get; set; }
        public string Tell { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Note { get; set; }
        public string OrgId { get; set; }
        //public string OrgCode { get; set; }
        public string OrgName { get; set; }
        public string State { get; set; }
        public string OrgApplication { get; set; }
        public string ApplicationIdx { get; set; }
        public string ExamNames { get; set; }
        public string Pers { get; set; }
        public string Budget { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedId { get; set; }
        public List<ReportApplicationOrg> ApplicationOrgs { get; set; }

        public DataTable tbOrgApplication { get; set; }
        public DataTable tbOrgPart { get; set; }

        //행정담당자 정보 추가
        public string b_Name { get; set; }
        public string b_Tell { get; set; }
        public string b_Email { get; set; }

        public string StateName { get; set; }
        public string BudgetName { get; set; }
        public string StateNameBrief { get; set; }

        public bool IsCheck { get; set; }
        public List<ReportStudent> Students { get; set; }
        public List<ReportDefaultInfo> Defaults { get; set; }
    }
}
