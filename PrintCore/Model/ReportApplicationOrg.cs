﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintCore
{
    public class ReportApplicationOrg
    {
        //TheShcool검사
        //public string OrgCode { get; set; }        
        public string OrgId { get; set; }
        public string OrgName { get; set; }
        public string OrgIdx { get; set; }
        public string ApplicationIdx { get; set; }
        public string Per { get; set; }
        public string RealPer { get; set; }
        public string ExamDate { get; set; }
        public string OrgType { get; set; }
        public string ExamCode { get; set; }
        public string HowTo { get; set; }
        public string CompletedCnt { get; set; } = "0";
        public string AuthCode { get; set; }
        public string Price { get; set; }
        public string TotalPrice { get; set; }
        public string DateCreated { get; set; }
        public string Pdf { get; set; }


        public string HowToName { get; set; }
        public string HowToNameBrief { get; set; }
        public string ExamName { get; set; }
    }
}
