﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintCore
{
    public class ReportDefaultInfo
    {
        public string StuIdx { get; set; }
        public string OrgIdx { get; set; }
        public string ExamCode { get; set; }
        public string ExamSubCode { get; set; }
        public string Answers { get; set; }
        public bool Completed { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string AimLink { get; set; }
        public string StuName { get; set; }
        public string OrgName { get; set; }
        public string ExamName { get; set; }
        public string Time { get; set; }
        public string SubTitle { get; set; }
        public string Contents { get; set; }
        public int QuestionCnt { get; set; }

        public string SchoolYear { get; set; }
        public string Class { get; set; }
        public string StuNumber { get; set; }
        public string ReportLink { get; set; }

        public bool IsCheck { get; set; }
    }
}
