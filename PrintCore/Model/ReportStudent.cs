﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PrintCore
{
    public class ReportStudent : ICloneable
    {
        public string OrgId { get; set; }
        public string OrgIdx { get; set; }
        public string StuIdx { get; set; }
        public string OrgName { get; set; }
        public string SchoolYear { get; set; }
        public string Class { get; set; }
        public string StuNumber { get; set; }
        public string StuName { get; set; }
        public string Password { get; set; }
        public string ExamCode { get; set; }
        public string AuthCode { get; set; }
        public string Progress { get; set; }
        public string Gender { get; set; }
        public bool Completed { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

        public bool IsCheck { get; set; }

        public char Parent { get; set; }

        public object Clone()
        {
            ReportStudent cloneObj = new ReportStudent();

            Type type = this.GetType();
            object objValue = Activator.CreateInstance(this.GetType());

            FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (FieldInfo field in fields)
            {
                field.SetValue(cloneObj, field.GetValue(this));
            }

            return cloneObj;
        }

    }
}
