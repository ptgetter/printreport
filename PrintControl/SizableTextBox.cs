﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace PrintControl
{
    public class SizableTextBox : TextBox
    {
        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hwnd, int msg, int wParam, IntPtr lParam);
        private const int EM_SETMARGINS = 0xd3;
        private const int EM_GETRECT = 0x00b2;
        private const int EM_SETRECT = 0x00b3;
        private const int EC_RIGHTMARGIN = 2;
        private const int EC_LEFTMARGIN = 1;
        private int p = 20;

        [DefaultValue(false)]
        [Browsable(true)]
        public override bool AutoSize
        {
            get { return base.AutoSize; }
            set { base.AutoSize = false; }
        }

        public SizableTextBox()
        {
            //var b = new Label { Dock = DockStyle.Bottom, Height = 8, BackColor = System.Drawing.Color.White };
            //var t = new Label { Dock = DockStyle.Top, Height = 8, BackColor = System.Drawing.Color.White };
            this.AutoSize = false;

            //Padding = new Padding(0);
            //Controls.AddRange(new Control[] { t });
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            //SetMargin();
        }
        private void SetMargin()
        {
            //IntPtr lparam = IntPtr.Zero;
            //SendMessage(Handle, EM_GETRECT, 0, lparam);




            //SendMessage(Handle, EM_SETMARGINS, EC_RIGHTMARGIN, p << 16);
            //SendMessage(Handle, EM_SETMARGINS, EC_LEFTMARGIN, p);
        }
    }
}
