﻿using PrintCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PrintControl
{
    public partial class StudentView : UserControl
    {
        public event ViewBrowserHandler OnBrowserEvent;
        public event CheckBoxHandler CheckEvent;
        LayerListView studentListView;

        public StudentView()
        {
            InitializeComponent();
            InitListView();
        }

        public void InitListView()
        {
            studentListView = new LayerListView();
            this.Controls.Add(studentListView);
            studentListView.Location = new Point(3, 3);
            studentListView.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
            studentListView.Size = new Size(this.Size.Width - 6, this.Size.Height - 6);
            studentListView.Scrollable = true;
            studentListView.View = View.Details;
            studentListView.FullRowSelect = true;
            studentListView.CheckBoxes = true;

            studentListView.ColumnClick += StudentListView_ColumnClick;
            studentListView.ItemChecked += StudentListView_ItemChecked;

            studentListView.MouseUp += StudentListView_MouseUp;
            studentListView.MouseMove += StudentListView_MouseMove;
            studentListView.OwnerDraw = true;
            studentListView.DrawColumnHeader += StudentListView_DrawColumnHeader;
            studentListView.DrawItem += StudentListView_DrawItem;
            studentListView.DrawSubItem += StudentListView_DrawSubItem;

            ColumnHeader columnheader = new ColumnHeader();
            columnheader.Text = "";
            columnheader.Width = 30;
            columnheader.TextAlign = HorizontalAlignment.Center;
            studentListView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "번호";
            columnheader.Width = 40;
            columnheader.TextAlign = HorizontalAlignment.Center;
            studentListView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "학년";
            columnheader.Width = 40;
            columnheader.TextAlign = HorizontalAlignment.Center;
            studentListView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "반";
            columnheader.Width = 40;
            columnheader.TextAlign = HorizontalAlignment.Center;
            studentListView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "번호";
            columnheader.Width = 40;
            columnheader.TextAlign = HorizontalAlignment.Center;
            studentListView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "이름";
            columnheader.Width = 60;
            columnheader.TextAlign = HorizontalAlignment.Center;
            studentListView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "보기";
            columnheader.Width = 60;
            columnheader.TextAlign = HorizontalAlignment.Center;
            studentListView.Columns.Add(columnheader);
        }

        private void StudentListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            bool isAll = true;
            foreach (ListViewItem item in studentListView.Items)
            {
                if (!item.Checked)
                {
                    isAll = false;
                }
            }

            if (isAll)
            {
                studentListView.Columns[0].Tag = true;
            }
            else
            {
                studentListView.Columns[0].Tag = false;
            }

            CheckEvent?.Invoke(e.Item.Tag?.ToString(), e.Item.Checked, isAll);

            studentListView.Invalidate(true);
        }

        private void StudentListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column == 0)
            {
                bool value = false;
                try
                {
                    value = Convert.ToBoolean(studentListView.Columns[e.Column].Tag);
                }
                catch { }
                studentListView.Columns[e.Column].Tag = !value;
                foreach (ListViewItem item in studentListView.Items)
                {
                    item.Checked = !value;
                }

                studentListView.Invalidate();
            }
        }

        private void StudentListView_MouseMove(object sender, MouseEventArgs e)
        {
            ListViewItem lvItem = this.studentListView.GetItemAt(e.X, e.Y);
            if (lvItem != null)
            {
                int minX = this.studentListView.Columns[0].Width + this.studentListView.Columns[1].Width + this.studentListView.Columns[2].Width + this.studentListView.Columns[3].Width + this.studentListView.Columns[4].Width + this.studentListView.Columns[5].Width;
                int maxX = minX + this.studentListView.Columns[6].Width;

                if (lvItem.Bounds.Height < e.Y && minX < e.X && maxX > e.X)
                {
                    this.Cursor = Cursors.Hand;
                }
                else
                {
                    this.Cursor = Cursors.Default;
                }
            }
            else
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void StudentListView_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.DrawDefault = true;
            }
            else
            {
                ListView lView = sender as ListView;
                TextFormatFlags flags = StaticMethod.GetTextAlignment(lView, e.ColumnIndex);
                Color itemColor = e.SubItem.ForeColor;
                TextRenderer.DrawText(e.Graphics, e.SubItem.Text, e.SubItem.Font, e.Bounds, itemColor, flags);
            }
        }

        private void StudentListView_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            if (e.Item.Selected)
            {
                Rectangle rect = e.Bounds;
                rect.X += 30;
                rect.Width -= 30;

                using (SolidBrush blue = new SolidBrush(Color.FromArgb(0, 120, 215)))
                {
                    e.Graphics.FillRectangle(blue, rect);
                }
            }
            else
            {
                e.Graphics.FillRectangle(Brushes.Transparent, e.Bounds);
            }
        }

        private void StudentListView_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.DrawBackground();
                bool value = false;
                try
                {
                    value = Convert.ToBoolean(e.Header.Tag);
                }
                catch { }

                CheckBoxRenderer.DrawCheckBox(e.Graphics, new Point(e.Bounds.Left + 4, e.Bounds.Top + 4), value ? System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal : System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal);
            }
            else
            {
                e.DrawDefault = true;
            }
        }

        private void StudentListView_MouseUp(object sender, MouseEventArgs e)
        {
            ListViewItem lvItem = this.studentListView.GetItemAt(e.X, e.Y);
            if (lvItem != null)
            {
                int minX = this.studentListView.Columns[0].Width + this.studentListView.Columns[1].Width + this.studentListView.Columns[2].Width + this.studentListView.Columns[3].Width + this.studentListView.Columns[4].Width + this.studentListView.Columns[5].Width;
                int maxX = minX + this.studentListView.Columns[6].Width;

                if (lvItem.Bounds.Height < e.Y && minX < e.X && maxX > e.X)
                {
                    if (OnBrowserEvent != null)
                    {
                        OnBrowserEvent(0, lvItem.Index);
                    }
                }
            }
        }

        public void SetData(List<ReportStudent> data)
        {
            studentListView.Items.Clear();
            int number = 1;
            foreach (var item in data)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Tag = item.StuIdx;
                lvi.Checked = item.IsCheck;
                lvi.SubItems.Add(number.ToString());
                lvi.SubItems.Add(item.SchoolYear?.Trim() ?? "");
                lvi.SubItems.Add(item.Class?.Trim() ?? "");
                lvi.SubItems.Add(item.StuNumber?.Trim() ?? "");
                lvi.SubItems.Add(item.StuName?.Trim() ?? "");
                if (item.Parent == 'F')
                {
                    lvi.SubItems.Add("부 [보기]");
                }
                else if (item.Parent == 'M')
                {
                    lvi.SubItems.Add("모 [보기]");
                }
                else
                {
                    lvi.SubItems.Add("[보기]");
                }
                lvi.SubItems[6].Font = new Font(lvi.SubItems[6].Font, FontStyle.Underline);
                lvi.SubItems[6].ForeColor = Color.BlueViolet;

                studentListView.Items.Add(lvi);
                number++;
            }

            if (data.Any(x => x.IsCheck == false))
            {
                studentListView.Columns[0].Tag = false;
            }
            else
            {
                studentListView.Columns[0].Tag = true;
            }

            studentListView.Invalidate(true);
        }

        public void RefreshMarker(bool check)
        {
            studentListView.Columns[0].Tag = check;

            for (int i = 0; i < studentListView.Items.Count; i++)
            {
                studentListView.Items[i].Checked = check;
            }

            studentListView.Invalidate(true);
        }

        public string GetSelectInfo()
        {
            if (studentListView.SelectedItems.Count > 0)
            {
                return studentListView.SelectedItems[0].Tag.ToString();
            }
            else
            {
                return null;
            }
        }
    }
}
