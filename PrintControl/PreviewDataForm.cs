﻿using PrintCore;
using System;
using System.Windows.Forms;

namespace PrintControl
{
    public partial class PreviewDataForm : Form
    {
        public event ActiveFormHandler BringToBackForm;
        CustomWebBrowser browser;
        public CustomWebBrowser Browser
        {
            get { return browser; }
        }

        public PreviewDataForm()
        {
            InitializeComponent();

            browser = new CustomWebBrowser(true);
            this.Controls.Add(browser);
            browser.Dock = DockStyle.Fill;

        }

        private void PreviewDataForm_Activated(object sender, EventArgs e)
        {
            BringToBackForm?.Invoke();
        }
    }
}
