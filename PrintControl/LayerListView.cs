﻿using PrintCore;
using System.ComponentModel;
using System.Windows.Forms;

namespace PrintControl
{
    public class LayerListView : ListView
    {
        public event ScrollHandler Scroll;
        private System.ComponentModel.Container components = null;

        public LayerListView()
        {
            InitializeComponent();
            this.Scrollable = true;
            DoubleBuffered = true;
        }

        protected virtual void OnScroll()
        {
            this.Scroll?.Invoke();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            components = new Container();
        }

        private const int WM_HSCROLL = 0x114;
        private const int WM_VSCROLL = 0x115;
        private const int WM_MOUSEWHEEL = 0x20a;

        protected override void WndProc(ref Message m)
        {
            if ((m.Msg == WM_VSCROLL) || (m.Msg == WM_HSCROLL))
            {
                this.Focus();
            }

            if (m.Msg == WM_VSCROLL || m.Msg == WM_MOUSEWHEEL)
            {
                OnScroll();
            }

            base.WndProc(ref m);
        }
    }
}
