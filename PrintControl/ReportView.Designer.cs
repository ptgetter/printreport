﻿namespace PrintControl
{
    partial class ReportView
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.loadingTB = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // loadingTB
            // 
            this.loadingTB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.loadingTB.AutoSize = true;
            this.loadingTB.BackColor = System.Drawing.Color.SkyBlue;
            this.loadingTB.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.loadingTB.Location = new System.Drawing.Point(233, 210);
            this.loadingTB.Name = "loadingTB";
            this.loadingTB.Size = new System.Drawing.Size(114, 24);
            this.loadingTB.TabIndex = 0;
            this.loadingTB.Text = "조회 중...";
            this.loadingTB.Visible = false;
            // 
            // ReportView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.loadingTB);
            this.Name = "ReportView";
            this.Size = new System.Drawing.Size(571, 453);
            this.SizeChanged += new System.EventHandler(this.ReportView_SizeChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label loadingTB;
    }
}
