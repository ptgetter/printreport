﻿using System.Windows.Forms;

namespace PrintControl
{
    public partial class LoadingForm : Form
    {
        public string LoadingTitle
        {
            get
            {
                return label1.Text;
            }
            set
            {
                label1.Text = value;
            }
        }

        public LoadingForm()
        {
            InitializeComponent();

        }
    }
}
