﻿using PrintCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PrintControl
{
    public partial class ReportView : UserControl
    {
        public event ScrollBottomHandler BottomEvent;
        public event SelectIndexHandler SelectEvent;
        public event CheckBoxHandler CheckEvent;
        LayerListView applicationListView;
        int sortColumn = -1;
        bool isAdmin = false;

        public ReportView()
        {
            InitializeComponent();

            InitListView();
        }

        public void InitListView()
        {
            applicationListView = new LayerListView();
            this.Controls.Add(applicationListView);
            applicationListView.Location = new Point(3, 3);
            applicationListView.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
            applicationListView.Size = new Size(this.Size.Width - 6, this.Size.Height - 6);
            applicationListView.Scrollable = true;
            applicationListView.View = View.Details;
            applicationListView.FullRowSelect = true;
            applicationListView.CheckBoxes = true;

            applicationListView.ItemSelectionChanged += ApplicationListView_ItemSelectionChanged;
            applicationListView.ColumnClick += ApplicationListView_ColumnClick;
            applicationListView.ItemChecked += ApplicationListView_ItemChecked;
            applicationListView.Scroll += ApplicationListView_Scroll;

            applicationListView.OwnerDraw = true;
            applicationListView.DrawColumnHeader += ApplicationListView_DrawColumnHeader;
            applicationListView.DrawItem += ApplicationListView_DrawItem;
            applicationListView.DrawSubItem += ApplicationListView_DrawSubItem;

            ColumnHeader columnheader = new ColumnHeader();
            columnheader.Text = "";
            columnheader.Width = 30;
            columnheader.TextAlign = HorizontalAlignment.Center;
            applicationListView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "번호";
            columnheader.Width = 40;
            columnheader.TextAlign = HorizontalAlignment.Center;
            applicationListView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "신청일자";
            columnheader.Width = 80;
            columnheader.TextAlign = HorizontalAlignment.Center;
            applicationListView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "진행현황";
            columnheader.Width = 60;
            columnheader.TextAlign = HorizontalAlignment.Center;
            applicationListView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "검사명";
            columnheader.Width = 120;
            columnheader.TextAlign = HorizontalAlignment.Center;
            applicationListView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "검사방법";
            columnheader.Width = 110;
            columnheader.TextAlign = HorizontalAlignment.Center;
            applicationListView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "인증코드";
            columnheader.Width = 80;
            columnheader.TextAlign = HorizontalAlignment.Center;
            applicationListView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "신청인원";
            columnheader.Width = 60;
            columnheader.TextAlign = HorizontalAlignment.Center;
            applicationListView.Columns.Add(columnheader);
        }

        private void ApplicationListView_Scroll()
        {
            if (applicationListView.Items[applicationListView.Items.Count - 1].Bounds.Y < applicationListView.Bounds.Height)
            {
                BottomEvent?.Invoke();
            }
        }

        private void ApplicationListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            CheckEvent?.Invoke(e.Item.Tag?.ToString(), e.Item.Checked, applicationListView.SelectedItems.Count > 0 && applicationListView.SelectedItems[0].Index == e.Item.Index ? true : false);
        }

        private void ApplicationListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column == 0)
            {
                bool value = false;
                try
                {
                    value = Convert.ToBoolean(applicationListView.Columns[e.Column].Tag);
                }
                catch { }
                applicationListView.Columns[e.Column].Tag = !value;
                foreach (ListViewItem item in applicationListView.Items)
                {
                    item.Checked = !value;
                }

                applicationListView.Invalidate();
            }
            else
            {
                if (e.Column != sortColumn)
                {
                    sortColumn = e.Column;
                    applicationListView.Sorting = SortOrder.Ascending;
                }
                else
                {
                    if (applicationListView.Sorting == SortOrder.Ascending)
                    {
                        applicationListView.Sorting = SortOrder.Descending;
                    }
                    else
                    {
                        applicationListView.Sorting = SortOrder.Ascending;
                    }
                }

                applicationListView.Sort();
                this.applicationListView.ListViewItemSorter = new ReportViewComparer(e.Column, applicationListView.Sorting);
            }
        }

        private void ApplicationListView_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.DrawDefault = true;
            }
            else
            {
                ListView lView = sender as ListView;
                TextFormatFlags flags = StaticMethod.GetTextAlignment(lView, e.ColumnIndex);
                Color itemColor = e.Item.ForeColor;
                TextRenderer.DrawText(e.Graphics, e.SubItem.Text, e.SubItem.Font, e.Bounds, itemColor, flags);
            }
        }

        private void ApplicationListView_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            if (e.Item.Selected)
            {
                Rectangle rect = e.Bounds;
                rect.X += 30;
                rect.Width -= 30;

                using (SolidBrush blue = new SolidBrush(Color.FromArgb(0, 120, 215)))
                {
                    e.Graphics.FillRectangle(blue, rect);
                }
            }
            else
            {
                e.Graphics.FillRectangle(Brushes.Transparent, e.Bounds);
            }

            //Rectangle rect = e.Bounds;
            ////rect.Y += 1;
            ////rect.Height -= 2;
            ////if (e.ItemIndex == 0)
            ////{
            ////    rect.Y += 1;
            ////    rect.Height -= 1;
            ////}
            ////else
            ////{
            ////    rect.Y -= 2;
            ////    rect.Height += 1;
            ////}
            ////if (e.ItemIndex == layerListView.Items.Count - 1)
            ////{
            ////    rect.Height += 2;
            ////}
            //Random rd = new Random((int)(DateTime.Now.Ticks * DateTime.Now.Ticks));

            //e.Graphics.DrawRectangle(new Pen(e.Item.BackColor, 1.0f), rect);
        }

        private void ApplicationListView_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.DrawBackground();
                bool value = false;
                try
                {
                    value = Convert.ToBoolean(e.Header.Tag);
                }
                catch { }

                CheckBoxRenderer.DrawCheckBox(e.Graphics, new Point(e.Bounds.Left + 4, e.Bounds.Top + 4), value ? System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal : System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal);
            }
            else
            {

                e.DrawDefault = true;
            }
        }

        private void ApplicationListView_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                SelectEvent?.Invoke(e.Item.Tag.ToString());
            }
        }

        public void SetData(List<ReportApplication> data)
        {
            //applicationListView.Clear();
            int number = applicationListView.Items.Count + 1;
            foreach (var item in data)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Tag = item.ApplicationOrgs.FirstOrDefault()?.OrgIdx;
                lvi.SubItems.Add(number.ToString());
                if (isAdmin)
                {
                    lvi.SubItems.Add(item.ApplicationOrgs.FirstOrDefault()?.OrgName);
                }
                lvi.SubItems.Add(item.DateCreated.ToShortDateString());
                lvi.SubItems.Add(item.StateNameBrief);
                lvi.SubItems.Add(item.ApplicationOrgs.FirstOrDefault()?.ExamName);
                lvi.SubItems.Add(item.ApplicationOrgs.FirstOrDefault()?.HowToNameBrief);
                lvi.SubItems.Add(item.ApplicationOrgs.FirstOrDefault()?.AuthCode);
                lvi.SubItems.Add(item.ApplicationOrgs.FirstOrDefault()?.Per);

                applicationListView.Items.Add(lvi);

                number++;
            }
        }

        public void ClearMarker(string currentIndex)
        {
            applicationListView.Columns[0].Tag = false;
            for (int i = 0; i < applicationListView.Items.Count; i++)
            {
                applicationListView.Items[i].Checked = false;

                CheckEvent?.Invoke(applicationListView.Items[i].Tag?.ToString(), false, applicationListView.SelectedItems.Count > 0 && applicationListView.SelectedItems[0].Tag.ToString() == currentIndex ? true : false, true);
            }

            applicationListView.Invalidate(true);
        }

        public void RefreshMarker(string orgIdx, bool check)
        {
            for (int i = 0; i < applicationListView.Items.Count; i++)
            {
                if (applicationListView.Items[i].Tag.ToString() == orgIdx)
                {
                    applicationListView.Items[i].Checked = check;
                }
            }

            //applicationListView.Invalidate(true);
        }

        public void ClearList()
        {
            applicationListView.Items.Clear();
        }

        public void SetAdmin()
        {
            isAdmin = true;

            ColumnHeader columnheader = new ColumnHeader();
            columnheader.Text = "학교명";
            columnheader.Width = 80;
            columnheader.TextAlign = HorizontalAlignment.Center;
            applicationListView.Columns.Insert(2, columnheader);
        }

        public void Loading(bool show)
        {
            this.Invoke(new Action(() =>
            {
                loadingTB.Visible = show;
                loadingTB.BringToFront();
            }));
        }

        private void ReportView_SizeChanged(object sender, EventArgs e)
        {
            loadingTB.Location = new Point(this.Width / 2 - loadingTB.Width / 2, this.Height / 2 - loadingTB.Height / 2);
        }
    }

    class ReportViewComparer : IComparer
    {
        private int col;
        private SortOrder order;
        public ReportViewComparer()
        {
            col = 0;
            order = SortOrder.Ascending;
        }

        public ReportViewComparer(int column, SortOrder order)
        {
            col = column;
            this.order = order;
        }

        public int Compare(object x, object y)
        {
            int returnVal = -1;
            int intX, intY;

            if (!string.IsNullOrEmpty(((ListViewItem)x).SubItems[col].Text) && int.TryParse(((ListViewItem)x).SubItems[col].Text, out intX) && int.TryParse(((ListViewItem)y).SubItems[col].Text, out intY))
            {
                if (intX > intY)
                {
                    returnVal = 1;
                }
                else if (intX < intY)
                {
                    returnVal = -1;
                }
                else
                {
                    returnVal = 0;
                }
            }
            else
            {
                returnVal = string.Compare(((ListViewItem)x).SubItems[col].Text, ((ListViewItem)y).SubItems[col].Text);
            }

            if (order == SortOrder.Descending)
            {
                returnVal *= -1;
            }

            return returnVal;
        }
    }
}
