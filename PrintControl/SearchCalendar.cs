﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace PrintControl
{
    public partial class SearchCalendar : UserControl
    {
        public string Title
        {
            get
            {
                return label1.Text;
            }
            set
            {
                label1.Text = value;
            }
        }

        public string DateString
        {
            get
            {
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        public SearchCalendar()
        {
            InitializeComponent();
        }

        public string GetSelectDate()
        {
            return textBox1.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            monthCalendar1.Show();
            this.Size = new Size(this.Size.Width, 203);
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            this.Size = new Size(this.Size.Width, 36);
            monthCalendar1.Hide();
            this.textBox1.Text = e.Start.ToShortDateString();
        }

        private void SearchCalendar_Leave(object sender, EventArgs e)
        {
            this.Size = new Size(this.Size.Width, 36);
            monthCalendar1.Hide();
        }
    }
}
