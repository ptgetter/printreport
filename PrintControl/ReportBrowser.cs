﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace PrintControl
{
    public partial class ReportBrowser : Form
    {
        CustomWebBrowser browser;
        bool isDispose = true;
        public CustomWebBrowser Browser
        {
            get { return browser; }
        }

        public ReportBrowser(bool closeDispose = true)
        {
            InitializeComponent();
            isDispose = closeDispose;

            this.FormClosing += ReportBrowser_FormClosing;

            browser = new CustomWebBrowser();
            browser.taskComplete += Browser_taskComplete;
            this.Controls.Add(browser);
            this.browser.Location = new Point(0, 27);
            this.browser.Size = new Size(1264, 419);
            this.browser.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

            this.Height = Screen.GetBounds(new Point()).Height - 100;
        }

        private void Browser_taskComplete(int type)
        {
            this.Invoke(new Action(() => {
                loadingLB.Visible = false;
            }));
        }

        private void ReportBrowser_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isDispose)
            {
                e.Cancel = true;
                this.Visible = false;
            }
        }

        private void saveFormatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "양식 파일|*.xml";
            sfd.OverwritePrompt = true;
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                if (browser.SaveFormat(sfd.FileName))
                {
                    MessageBox.Show("저장되었습니다.");
                }
                else
                {
                    MessageBox.Show("오류가 발생하였습니다.");
                }
            }
        }

        private void loadFormatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "양식 파일|*.xml";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                browser.LoadFormat(ofd.FileName);
            }
        }

        private void printToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            //string path = browser.SavePDF(Path.Combine(Environment.CurrentDirectory, "PDFOUTPUT"));

            //browser.SetPDFView(path, true);

            browser.CurrentPrint();
        }
    }
}
