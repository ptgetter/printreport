﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace PrintControl
{
    public partial class PreviewBackForm : Form
    {
        int bW;
        int bH;
        int fH;
        PreviewDataForm form1;
        PreviewMasterForm form2;

        bool bringToFront = false;

        public PreviewBackForm(string url, string dataFormat, string masterFormat)
        {
            InitializeComponent();

            this.Height = Screen.GetBounds(new Point()).Height - 100;

            form1 = new PreviewDataForm();
            form2 = new PreviewMasterForm();

            bW = 0;// SystemInformation.Border3DSize.Width;
            bH = 0;// SystemInformation.Border3DSize.Height;
            fH = SystemInformation.CaptionHeight;

            Rectangle rc = this.RectangleToScreen(this.ClientRectangle);

            form1.Show();
            form2.Show();

            form1.Location = rc.Location;
            form2.Location = rc.Location;
            form1.Size = rc.Size;
            form2.Size = rc.Size;

            form1.Browser.scrollChange += Browser_scrollChange;
            form2.Browser.scrollChange += Browser_scrollChange;
            form1.BringToBackForm += Form_BringToBackForm;
            form2.BringToBackForm += Form_BringToBackForm;

            form1.Browser.SetNavigation(url, 101, dataFormat);
            form2.Browser.LoadMasterFormat(url, masterFormat);


        }

        private void Form_BringToBackForm()
        {
            if (!bringToFront)
            {
                bringToFront = true;
                this.BringToFront();
            }

            Thread t1 = new Thread(() =>
            {
                Thread.Sleep(1000);
                bringToFront = false;
            });

            t1.Start();
        }

        private void Browser_scrollChange(int x, int y)
        {
            form1.Browser.SetScroll(x, y);
            form2.Browser.SetScroll(x, y);
        }

        private void Browser_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void PreviewBackForm_Move(object sender, EventArgs e)
        {
            if (form1 != null && form2 != null)
            {
                Rectangle rc = this.RectangleToScreen(this.ClientRectangle);

                form1.Location = rc.Location;
                form2.Location = rc.Location;
                form1.Size = rc.Size;
                form2.Size = rc.Size;
            }
        }

        private void PreviewBackForm_Resize(object sender, EventArgs e)
        {
            if (form1 != null && form2 != null)
            {
                Rectangle rc = this.RectangleToScreen(this.ClientRectangle);

                form1.Location = rc.Location;
                form2.Location = rc.Location;
                form1.Size = rc.Size;
                form2.Size = rc.Size;
            }
        }

        private void PreviewBackForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            form1.Close();
            form1.Dispose();
            form2.Close();
            form2.Dispose();
        }
    }
}
