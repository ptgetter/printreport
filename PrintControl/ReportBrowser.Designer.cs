﻿namespace PrintControl
{
    partial class ReportBrowser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadFormatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFormatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadingLB = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1264, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일ToolStripMenuItem
            // 
            this.파일ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadFormatToolStripMenuItem,
            this.saveFormatToolStripMenuItem,
            this.printToolStripMenuItem});
            this.파일ToolStripMenuItem.Name = "파일ToolStripMenuItem";
            this.파일ToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.파일ToolStripMenuItem.Text = "파일";
            // 
            // loadFormatToolStripMenuItem
            // 
            this.loadFormatToolStripMenuItem.Name = "loadFormatToolStripMenuItem";
            this.loadFormatToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.loadFormatToolStripMenuItem.Text = "양식 불러오기";
            this.loadFormatToolStripMenuItem.Click += new System.EventHandler(this.loadFormatToolStripMenuItem_Click);
            // 
            // saveFormatToolStripMenuItem
            // 
            this.saveFormatToolStripMenuItem.Name = "saveFormatToolStripMenuItem";
            this.saveFormatToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.saveFormatToolStripMenuItem.Text = "양식 저장하기";
            this.saveFormatToolStripMenuItem.Click += new System.EventHandler(this.saveFormatToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.printToolStripMenuItem.Text = "인쇄";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // loadingLB
            // 
            this.loadingLB.BackColor = System.Drawing.Color.Transparent;
            this.loadingLB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loadingLB.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.loadingLB.Location = new System.Drawing.Point(0, 24);
            this.loadingLB.Name = "loadingLB";
            this.loadingLB.Size = new System.Drawing.Size(1264, 426);
            this.loadingLB.TabIndex = 2;
            this.loadingLB.Text = "불러오는 중...";
            this.loadingLB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ReportBrowser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 450);
            this.Controls.Add(this.loadingLB);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimizeBox = false;
            this.Name = "ReportBrowser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ReportBrowser";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadFormatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveFormatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.Label loadingLB;
    }
}