﻿using PrintCore;
using System;
using System.Windows.Forms;

namespace PrintControl
{
    public partial class LoginForm : Form
    {
        public bool isLogin;
        public bool isAdmin = false;

        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (DialogResult != DialogResult.OK)
            {
                DialogResult = DialogResult.Cancel;
            }
        }

        private void pwdTB_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                Login();
            }
        }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void Login()
        {
            if (string.IsNullOrEmpty(idTB.Text))
            {
                MessageBox.Show("아이디를 입력해 주세요.");
            }
            else if (string.IsNullOrEmpty(pwdTB.Text))
            {
                MessageBox.Show("암호를 입력해 주세요.");
            }
            else if (WebUtil.Instance.Login(idTB.Text, pwdTB.Text))
            {
                isLogin = WebUtil.Instance.isLogin;
                isAdmin = idTB.Text.ToLower() == "super" ? true : false;
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show("로그인에 실패하였습니다.");
            }
        }
    }
}
