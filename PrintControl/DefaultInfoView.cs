﻿using PrintCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace PrintControl
{
    public partial class DefaultInfoView : UserControl
    {
        public event ViewBrowserHandler OnBrowserEvent;
        public event CheckBoxHandler CheckEvent;
        LayerListView defaultInfoView;

        public DefaultInfoView()
        {
            InitializeComponent();
            InitListView();
        }

        public void InitListView()
        {
            defaultInfoView = new LayerListView();
            this.Controls.Add(defaultInfoView);
            defaultInfoView.Location = new Point(3, 3);
            defaultInfoView.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Bottom | AnchorStyles.Right;
            defaultInfoView.Size = new Size(this.Size.Width - 6, this.Size.Height - 6);
            defaultInfoView.Scrollable = true;
            defaultInfoView.View = View.Details;
            defaultInfoView.FullRowSelect = true;
            defaultInfoView.CheckBoxes = true;

            defaultInfoView.ColumnClick += DefaultInfoView_ColumnClick;
            defaultInfoView.ItemChecked += DefaultInfoView_ItemChecked;

            defaultInfoView.MouseUp += DefaultInfoView_MouseUp;
            defaultInfoView.MouseMove += DefaultInfoView_MouseMove;
            defaultInfoView.OwnerDraw = true;
            defaultInfoView.DrawColumnHeader += DefaultInfoView_DrawColumnHeader;
            defaultInfoView.DrawItem += DefaultInfoView_DrawItem;
            defaultInfoView.DrawSubItem += DefaultInfoView_DrawSubItem;

            ColumnHeader columnheader = new ColumnHeader();
            columnheader.Text = "";
            columnheader.Width = 30;
            columnheader.TextAlign = HorizontalAlignment.Center;
            defaultInfoView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "번호";
            columnheader.Width = 70;
            columnheader.TextAlign = HorizontalAlignment.Center;
            defaultInfoView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "학년";
            columnheader.Width = 70;
            columnheader.TextAlign = HorizontalAlignment.Center;
            defaultInfoView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "반";
            columnheader.Width = 70;
            columnheader.TextAlign = HorizontalAlignment.Center;
            defaultInfoView.Columns.Add(columnheader);
            columnheader = new ColumnHeader();
            columnheader.Text = "통계";
            columnheader.Width = 70;
            columnheader.TextAlign = HorizontalAlignment.Center;
            defaultInfoView.Columns.Add(columnheader);
        }

        private void DefaultInfoView_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.DrawDefault = true;
            }
            else
            {
                ListView lView = sender as ListView;
                TextFormatFlags flags = StaticMethod.GetTextAlignment(lView, e.ColumnIndex);
                Color itemColor = e.SubItem.ForeColor;
                TextRenderer.DrawText(e.Graphics, e.SubItem.Text, e.SubItem.Font, e.Bounds, itemColor, flags);
            }
        }

        private void DefaultInfoView_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            if (e.Item.Selected)
            {
                Rectangle rect = e.Bounds;
                rect.X += 30;
                rect.Width -= 30;

                using (SolidBrush blue = new SolidBrush(Color.FromArgb(0, 120, 215)))
                {
                    e.Graphics.FillRectangle(blue, rect);
                }
            }
            else
            {
                e.Graphics.FillRectangle(Brushes.Transparent, e.Bounds);
            }
        }

        private void DefaultInfoView_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                e.DrawBackground();
                bool value = false;
                try
                {
                    value = Convert.ToBoolean(e.Header.Tag);
                }
                catch { }

                CheckBoxRenderer.DrawCheckBox(e.Graphics, new Point(e.Bounds.Left + 4, e.Bounds.Top + 4), value ? System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal : System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal);
            }
            else
            {
                e.DrawDefault = true;
            }
        }

        private void DefaultInfoView_MouseMove(object sender, MouseEventArgs e)
        {
            ListViewItem lvItem = this.defaultInfoView.GetItemAt(e.X, e.Y);
            if (lvItem != null)
            {
                int minX = this.defaultInfoView.Columns[0].Width + this.defaultInfoView.Columns[1].Width + this.defaultInfoView.Columns[2].Width + this.defaultInfoView.Columns[3].Width;
                int maxX = minX + this.defaultInfoView.Columns[4].Width;

                if (lvItem.Bounds.Height < e.Y && minX < e.X && maxX > e.X)
                {
                    this.Cursor = Cursors.Hand;
                }
                else
                {
                    this.Cursor = Cursors.Default;
                }
            }
            else
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void DefaultInfoView_MouseUp(object sender, MouseEventArgs e)
        {
            ListViewItem lvItem = this.defaultInfoView.GetItemAt(e.X, e.Y);
            if (lvItem != null)
            {
                int minX = this.defaultInfoView.Columns[0].Width + this.defaultInfoView.Columns[1].Width + this.defaultInfoView.Columns[2].Width + this.defaultInfoView.Columns[3].Width;
                int maxX = minX + this.defaultInfoView.Columns[4].Width;

                if (lvItem.Bounds.Height < e.Y && minX < e.X && maxX > e.X)
                {

                    OnBrowserEvent?.Invoke(1, lvItem.Index);
                }
            }
        }

        private void DefaultInfoView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            bool isAll = true;
            foreach (ListViewItem item in defaultInfoView.Items)
            {
                if (item == null || !item.Checked)
                {
                    isAll = false;
                }
            }

            if (isAll)
            {
                defaultInfoView.Columns[0].Tag = true;
            }
            else
            {
                defaultInfoView.Columns[0].Tag = false;
            }

            CheckEvent?.Invoke(e.Item.Tag?.ToString(), e.Item.Checked, isAll);

            defaultInfoView.Invalidate(true);
        }

        private void DefaultInfoView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column == 0)
            {
                bool value = false;
                try
                {
                    value = Convert.ToBoolean(defaultInfoView.Columns[e.Column].Tag);
                }
                catch { }
                defaultInfoView.Columns[e.Column].Tag = !value;
                foreach (ListViewItem item in defaultInfoView.Items)
                {
                    item.Checked = !value;
                }

                defaultInfoView.Invalidate();
            }
        }

        public void SetData(List<ReportDefaultInfo> data)
        {
            defaultInfoView.Items.Clear();
            int number = 1;

            foreach (var item in data)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Tag = string.Format("{0}|{1}|{2}", item.OrgIdx, item.SchoolYear, item.Class);
                lvi.Checked = item.IsCheck;
                lvi.SubItems.Add(number.ToString());
                lvi.SubItems.Add(item.SchoolYear?.Trim() ?? "");
                lvi.SubItems.Add(item.Class?.Trim() ?? "");
                lvi.SubItems.Add("[보기]");
                lvi.SubItems[4].Font = new Font("굴림", 9f, FontStyle.Underline);
                lvi.SubItems[4].ForeColor = Color.BlueViolet;

                defaultInfoView.Items.Add(lvi);
                number++;
            }

            if (data.Any(x => x.IsCheck == false))
            {
                defaultInfoView.Columns[0].Tag = false;
            }
            else
            {
                defaultInfoView.Columns[0].Tag = true;
            }

            defaultInfoView.Invalidate(true);
        }

        public void RefreshMarker(bool check)
        {
            defaultInfoView.Columns[0].Tag = check;

            for (int i = 0; i < defaultInfoView.Items.Count; i++)
            {
                defaultInfoView.Items[i].Checked = check;
            }

            defaultInfoView.Invalidate(true);
        }

        public string GetSelectInfo()
        {
            if (defaultInfoView.SelectedItems.Count > 0)
            {
                return defaultInfoView.SelectedItems[0].Tag.ToString();
            }
            else
            {
                return null;
            }
        }
    }
}
