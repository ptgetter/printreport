﻿using CefSharp;
using CefSharp.WinForms;
using Newtonsoft.Json.Linq;
using PrintCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace PrintControl
{
    public partial class CustomWebBrowser : UserControl
    {
        public event ScrollPositionHandler scrollChange;
        public event LoadBrowserHandler ShowBrowser;
        public event BrowserTaskCompleteHandler taskComplete;  // 1: 브라우져 로드 완료, 2: pdf 저장 완료
        HtmlAgilityPack.HtmlDocument htmlDoc;
        string reportURL;
        int browserLoadType;    // 0x1: 오리지널 HTML 로드, 0x2: 기본 스타일,스크립트 추가, 0x4: 지정된 양식 추가, 0x8: 화면 띄우기, 0x10: InitScript 실행, 0x20:  Json 데이터 로드, 0x40: InitPreview 실행, 0x80: 프린트
        string loadStyle;
        string loadScript;
        string currentFormatPath;
        string currentUrl;

        ExterenJsFunc externJs;
        List<string> iframeURL;

        bool alertSilent = false;

        public CustomWebBrowser(bool loadExtern = false)
        {
            if (!Cef.IsInitialized)
            {
                var settings = new CefSettings();
                settings.RegisterScheme(new CefCustomScheme
                {
                    SchemeName = CustomProtocolSchemeHandlerFactory.SchemeName,
                    SchemeHandlerFactory = new CustomProtocolSchemeHandlerFactory(),
                    IsCSPBypassing = true
                });

                settings.LogSeverity = LogSeverity.Default;
                Cef.Initialize(settings);
            }

            InitializeComponent();

            if (loadExtern)
            {
                externJs = new ExterenJsFunc();
                externJs.scrollChange += ExternJs_scrollChange;
                browser.JavascriptObjectRepository.Settings.LegacyBindingEnabled = true;
                browser.JavascriptObjectRepository.Register("externJs", externJs, isAsync: true);
            }

            reportURL = ConfigurationManager.AppSettings["ReportURL"];
            browser.FrameLoadEnd += Browser_FrameLoadEnd;

            BrowserSettings bs = new BrowserSettings();
            bs.FileAccessFromFileUrls = CefState.Enabled;
            bs.UniversalAccessFromFileUrls = CefState.Enabled;

            browser.BrowserSettings = bs;

            iframeURL = new List<string>();
        }

        private void Browser_FrameLoadEnd(object sender, CefSharp.FrameLoadEndEventArgs e)
        {
            try
            {
                Console.WriteLine(e.Frame.Url);
                if (e.Frame.IsMain)
                {
                    if ((browserLoadType & 0x10) > 0)
                    {
                        browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync("InitScript()");
                    }

                    if ((browserLoadType & 0x20) > 0 && !string.IsNullOrEmpty(currentFormatPath))
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.Load(currentFormatPath);

                        string dataJson = xmlDoc.SelectSingleNode("//root/json").InnerText;
                        browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync(string.Format("initLoadJson(JSON.stringify({0}))", dataJson));
                    }

                    if ((browserLoadType & 0x40) > 0)
                    {
                        browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync("InitPreview()");
                    }

                    if ((browserLoadType & 0x80) > 0)
                    {
                        Thread printT = new Thread(() =>
                        {
                            Thread.Sleep(4000);
                            CefSharp.WebBrowserExtensions.Print(browser);
                        });
                        printT.Start();
                    }

                    //browserLoadType = 0;
                    taskComplete?.Invoke(1);
                }
                else if (iframeURL?.Any(x => e.Frame.Url.Contains(x.Replace("\\", "/"))) == true)
                {
                    // iframe 처리
                    if ((browserLoadType & 0x20) > 0 && !string.IsNullOrEmpty(currentFormatPath))
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.Load(currentFormatPath);

                        int curIndex = iframeURL.FindIndex(x => e.Frame.Url.Contains(x.Replace("\\", "/")));

                        string dataJson = xmlDoc.SelectNodes("//root/iframes").Item(curIndex).InnerText;
                        browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync(string.Format("initLoadJsoniFrame({0}, JSON.stringify({1}))", curIndex, dataJson));
                    }

                    if ((browserLoadType & 0x40) > 0)
                    {
                        browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync("InitPreview()");
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtil.Instance.WriteTextLog("Browser_FrameLoadEnd", ex);
                if (!alertSilent)
                {
                    MessageBox.Show("페이지 양식 처리 중 오류가 발생하였습니다.");
                }
                taskComplete?.Invoke(3);
            }
        }

        public bool SetNavigation(string url, int loadType, string formatPath = null)
        {
            alertSilent = false;
            iframeURL?.Clear();
            currentUrl = url;
            if (!string.IsNullOrEmpty(formatPath))
            {
                currentFormatPath = formatPath;
            }

            browserLoadType = loadType;

            return RequestPage(string.Concat(reportURL, url));
        }

        public bool SetNavigationSilent(string url, int loadType, string formatPath)
        {
            alertSilent = true;
            iframeURL?.Clear();
            currentFormatPath = formatPath;
            browserLoadType = loadType;
            //browserLoadType = 5;
            return RequestPage(string.Concat(reportURL, url));
        }

        public void SetPDFView(string path, bool isPrint)
        {
            if (isPrint)
            {
                browserLoadType = 128;
            }

            browser.Load(string.Concat("local:", path));
        }

        public void CurrentPrint()
        {
            browser.Print();
        }

        /// <summary>
        ///  PDF로 저장
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        public string SavePDF(string filePath, string fileName = null)
        {
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }

            Thread.Sleep(100);
            string pdfPath = Path.Combine(filePath, string.IsNullOrEmpty(fileName) ? DateTime.Now.Ticks.ToString() + ".pdf" : fileName);
            var taskAsync = Task.Run(async () => await CefSharp.WebBrowserExtensions.PrintToPdfAsync(browser.GetBrowser(), pdfPath));
            taskAsync.Wait();

            return pdfPath;
        }

        public void ReportPrint()
        {
            CefSharp.WebBrowserExtensions.Print(browser);
        }

        /// <summary>
        /// 스크립트 파일 읽어오기
        /// </summary>
        /// <returns></returns>
        public bool InitScript()
        {
            string scriptPath = ConfigurationManager.AppSettings["ScriptPath"];
            if (scriptPath.StartsWith("\\"))
            {
                scriptPath = scriptPath.Substring(1);
            }

            if (!Regex.IsMatch(scriptPath, @"^[A-Z]:\\"))
            {
                scriptPath = Path.Combine(Environment.CurrentDirectory, scriptPath);
            }

            if (File.Exists(scriptPath))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(scriptPath);
                loadStyle = doc.SelectSingleNode("root/style").InnerText.Replace("$[[LOCALPATH]]", Environment.CurrentDirectory.Replace(@"\", "/"));
                loadScript = doc.SelectSingleNode("root/script").InnerText;

                return true;
            }

            return false;
        }

        public void LoadFormat(string filePath)
        {
            iframeURL.Clear();
            currentFormatPath = filePath;
            browserLoadType = 53;
            RequestPage(string.Concat(reportURL, currentUrl));
        }

        public void LoadMasterFormat(string url, string filePath)
        {
            currentFormatPath = filePath;
            browserLoadType = 0;
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(filePath);

            //string dataHTML = xmlDoc.SelectSingleNode("//root/html").InnerText;

            //CefSharp.WebBrowserExtensions.LoadHtml(browser, dataHTML, string.Concat(reportURL, url), Encoding.UTF8);

            string pdfViewer = File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "PDFViewer.html"));
            pdfViewer = pdfViewer.Replace("[[PDFBINARYDATA]]", Convert.ToBase64String(File.ReadAllBytes(filePath)));

            CefSharp.WebBrowserExtensions.LoadHtml(browser, pdfViewer, string.Concat(reportURL, url), Encoding.UTF8);
        }

        public bool SaveFormat(string filePath)
        {
            try
            {
                var taskAsync = Task.Run(async () => await browser.GetBrowser().MainFrame.EvaluateScriptAsync("externGetFormat()"));
                taskAsync.Wait();

                var taskAsync2 = Task.Run(async () => await browser.GetBrowser().MainFrame.EvaluateScriptAsync("externIFrameFormat()"));
                taskAsync2.Wait();

                var jData = taskAsync2.Result.Result as List<Object>;
                
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.AppendChild(xmlDoc.CreateElement("root"));
                xmlDoc.DocumentElement.AppendChild(SetCDATA(xmlDoc, "json", taskAsync.Result.Result.ToString()));
                xmlDoc.DocumentElement.AppendChild(SetCDATA(xmlDoc, "style", htmlDoc.DocumentNode.SelectNodes("//html/head/style").Last().InnerText));
                xmlDoc.DocumentElement.AppendChild(SetCDATA(xmlDoc, "script", htmlDoc.DocumentNode.SelectNodes("//html/head/script").Last().InnerText));

                if (jData.Count > 0)
                {
                    XmlNode xmlNode = xmlDoc.CreateElement("iframes");
                    foreach (var item in jData)
                    {
                        xmlNode.AppendChild(SetCDATA(xmlDoc, "iframe", item.ToString()));
                    }
                    xmlDoc.DocumentElement.AppendChild(xmlNode);
                }

                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                xmlDoc.Save(filePath);

                return true;
            }
            catch (Exception ex)
            {
                LogUtil.Instance.WriteTextLog("SaveFormat", ex);
                return false;
            }
        }

        public void RealignPosition()
        {
            if (browser.IsBrowserInitialized)
            {
                browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync("positionChange()");
            }
        }

        public void SetScroll(int x, int y)
        {
            browser.GetBrowser().MainFrame.ExecuteJavaScriptAsync(string.Format("setScroll({0}, {1})", x, y));
        }

        private void ExternJs_scrollChange(int x, int y)
        {
            scrollChange?.Invoke(x, y);
        }

        private bool RequestPage(string url, string filePath = null)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Encoding encode = null;
                if (response.CharacterSet.ToLower() == "utf-8")
                {
                    encode = Encoding.UTF8;
                }
                else
                {
                    encode = Encoding.Default;
                }

                var curHTMLDoc = new HtmlAgilityPack.HtmlDocument();
                var curiframeURL = new List<string>();

                if ((browserLoadType & 0x1) > 0)
                {
                    bool isJquery = false;
                    curHTMLDoc.Load(response.GetResponseStream(), encode);

                    List<HtmlAgilityPack.HtmlNode> originNode = new List<HtmlAgilityPack.HtmlNode>();
                    List<HtmlAgilityPack.HtmlNode> replaceNode = new List<HtmlAgilityPack.HtmlNode>();

                    Regex regBGI = new Regex(@"(['|""]background-image['|""]\s*,\s*['|""]url\()(.*?['|""])");
                    foreach (var item in curHTMLDoc.DocumentNode.Descendants())
                    {
                        if (item.Name.ToLower() == "script" && item.Attributes["src"] == null)
                        {
                            originNode.Add(item);
                            replaceNode.Add(HtmlAgilityPack.HtmlNode.CreateNode(string.Concat("<script>", regBGI.Replace(item.InnerText, string.Format("$1{0}$2", reportURL)), "</script>")));
                        }
                        else if ((item.Name.ToLower() == "img" || item.Name.ToLower() == "script" || item.Name.ToLower() == "iframe") && item.Attributes["src"] != null && item.Attributes["src"].Value.Trim().StartsWith("http") == false)
                        {
                            item.SetAttributeValue("src", string.Concat(reportURL, item.Attributes["src"].Value));

                            if (item.Name.ToLower() == "script" && item.Attributes["src"]?.Value.ToLower().Contains("jquery") == true)
                            {
                                isJquery = true;
                            }
                            else if (item.Name.ToLower() == "iframe")
                            {
                                curiframeURL.Add(Path.Combine(Environment.CurrentDirectory, string.Format("iFrame{0}.html", curiframeURL.Count)));
                                RequestPage(item.GetAttributeValue("src", ""), curiframeURL.Last());
                                item.SetAttributeValue("src", curiframeURL.Last());
                            }
                        }
                        else if (item.Name.ToLower() == "link" && item.Attributes["href"]?.Value.Trim().StartsWith("http") == false)
                        {
                            item.SetAttributeValue("href", string.Concat(reportURL, item.Attributes["href"].Value));
                        }
                        else if (item.Attributes["style"] != null)
                        {
                            var styles = item.Attributes["style"].Value.Split(';');

                            for (int i = 0; i < styles.Length; i++)
                            {
                                if (styles[i].ToLower().Trim().StartsWith("background-image"))
                                {
                                    var sep = styles[i].Split(':');
                                    if (sep[1].ToLower().Contains("url(\""))
                                    {
                                        styles[i] = string.Format("{0}: url(\"{1}{2}", sep[0], reportURL, sep[1].Replace("url(\"", ""));
                                    }
                                    else if (sep[1].ToLower().Contains("url('"))
                                    {
                                        styles[i] = string.Format("{0}: url('{1}{2}", sep[0], reportURL, sep[1].Replace("url('", ""));
                                    }
                                    else if (sep[1].ToLower().Contains("url("))
                                    {
                                        styles[i] = string.Format("{0}: url({1}{2}", sep[0], reportURL, sep[1].Replace("url(", ""));
                                    }
                                }
                                else if (styles[i].ToLower().Trim().StartsWith("background"))
                                {
                                    if (styles[i].ToLower().Contains("url(\""))
                                    {
                                        var sep = styles[i].Split(new string[] { "url(\"" }, StringSplitOptions.RemoveEmptyEntries);
                                        styles[i] = string.Format("{0} url(\"{1}{2}", sep[0], reportURL, sep[1]);
                                    }
                                    else if (styles[i].ToLower().Contains("url('"))
                                    {
                                        var sep = styles[i].Split(new string[] { "url('" }, StringSplitOptions.RemoveEmptyEntries);
                                        styles[i] = string.Format("{0} url('{1}{2}", sep[0], reportURL, sep[1]);
                                    }
                                    else if (styles[i].ToLower().Contains("url("))
                                    {
                                        var sep = styles[i].Split(new string[] { "url(" }, StringSplitOptions.RemoveEmptyEntries);
                                        styles[i] = string.Format("{0} url({1}{2}", sep[0], reportURL, sep[1]);
                                    }
                                }
                            }

                            item.SetAttributeValue("style", string.Join(";", styles));
                        }
                    }

                    if (!isJquery)
                    {
                        HtmlAgilityPack.HtmlNode jqueryNode = HtmlAgilityPack.HtmlNode.CreateNode(@"<script type=""text/javascript"" src=""http://psy.futureplan.co.kr/resource/js/jquery-1.10.2.js""></script>");
                        var headNode = curHTMLDoc.DocumentNode.SelectSingleNode("//html/head");
                        headNode.InsertBefore(jqueryNode, headNode.LastChild);
                    }

                    if (originNode.Count > 0)
                    {
                        for (int i = 0; i < originNode.Count; i++)
                        {
                            originNode[i].ParentNode.ReplaceChild(replaceNode[i], originNode[i]);
                        }
                    }
                }

                if ((browserLoadType & 0x2) > 0)
                {
                    HtmlAgilityPack.HtmlNode styleNode = HtmlAgilityPack.HtmlNode.CreateNode(string.Concat("<style type='text/css'>", loadStyle, "</style>"));
                    HtmlAgilityPack.HtmlNode scriptNode = HtmlAgilityPack.HtmlNode.CreateNode(string.Concat("<script>", loadScript, "</script>"));

                    var headNode = curHTMLDoc.DocumentNode.SelectSingleNode("//html/head");

                    headNode.InsertAfter(styleNode, headNode.LastChild);
                    headNode.InsertAfter(scriptNode, headNode.LastChild);
                }
                else if ((browserLoadType & 0x4) > 0 && !string.IsNullOrEmpty(currentFormatPath))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(currentFormatPath);

                    HtmlAgilityPack.HtmlNode styleNode = HtmlAgilityPack.HtmlNode.CreateNode(string.Concat("<style>", xmlDoc.SelectSingleNode("//root/style").InnerText, "</style>"));
                    HtmlAgilityPack.HtmlNode scriptNode = HtmlAgilityPack.HtmlNode.CreateNode(string.Concat("<script>", xmlDoc.SelectSingleNode("//root/script").InnerText, "</script>"));

                    var headNode = curHTMLDoc.DocumentNode.SelectSingleNode("//html/head");

                    headNode.InsertAfter(styleNode, headNode.LastChild);
                    headNode.InsertAfter(scriptNode, headNode.LastChild);
                }

                //if ((browserLoadType & 8) > 0 && ShowBrowser != null)
                //{
                //    ShowBrowser();
                //}

                if ((browserLoadType & 7) > 0)
                {
                    if (string.IsNullOrEmpty(filePath))
                    {
                        curHTMLDoc.Save(Path.Combine(Environment.CurrentDirectory, "Report.html"));
                        htmlDoc = curHTMLDoc;
                        iframeURL.AddRange(curiframeURL);
                        browser.Load(Path.Combine(Environment.CurrentDirectory, "Report.html"));
                    }
                    else
                    {
                        curHTMLDoc.Save(filePath);
                    }

                    iframeURL.AddRange(curiframeURL);
                }

                return true;
            }
            catch (Exception ex)
            {
                LogUtil.Instance.WriteTextLog("Request Page", ex);
                if (!alertSilent)
                {
                    MessageBox.Show("리포트를 불러 오는 중 오류가 발생하였습니다.");
                }

                return false;
            }
        }

        private XmlElement SetCDATA(XmlDocument doc, string name, string data)
        {
            XmlElement element = doc.CreateElement(name);
            XmlCDataSection cData;
            cData = doc.CreateCDataSection(data);
            element.AppendChild(cData);

            return element;
        }

        private void browser_ClientSizeChanged(object sender, EventArgs e)
        {
            RealignPosition();
        }
    }

    public class ExterenJsFunc
    {
        public event ScrollPositionHandler scrollChange;
        public void OnScroll(int x, int y)
        {
            scrollChange?.Invoke(x, y);
        }
    }

    public class CustomProtocolSchemeHandlerFactory : ISchemeHandlerFactory
    {
        public const string SchemeName = "local";

        public IResourceHandler Create(IBrowser browser, IFrame frame, string schemeName, IRequest request)
        {
            var uri = new Uri(request.Url);
            var file = string.Format("{0}:{1}", uri.Authority, uri.AbsolutePath.Replace("/", @"\"));

            var fileExtension = Path.GetExtension(file);
            var mimeType = Cef.GetMimeType(fileExtension);
            return ResourceHandler.FromFilePath(file, mimeType);

        }
    }
}
